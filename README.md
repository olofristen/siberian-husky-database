# - PROJECT IN PROGRESS - #

## Creator ##
**Owner & Creator:** Ólöf Gyða Risten Svansdóttir (olofristen@gmail.com)

## About the project ##
When this project was created the "database" of the Siberian Husky Club of Iceland was made out of hundreds of HTML pages where updating was time consuming and little changes could take a lot of time as dogs needed to be updated everywhere they appeared in different pages.

This project was created to eliminate this so the database would be easy to update and make it easy for Siberian Husky owners to search the database.

## To Run the Project ##
To run the client.
Be in the 'Client' directory and write:
```
ng build
npm start
```

To run the server (API, Repositories, Services).
Be in the 'Server/API' directory and write:
```
dotnet build
dotnet watch run
```

By doing both steps as above you can have the client and the server running and as you save your files they will automatically build and show the changes.
The localhost you should use in your browser is:
```
http://localhost:4200/
```

## To Change the Structure of the Database ##
To change the structure of the database you first need to change the Entity model for that table. When that is done you should be in
the 'Server/API' directory and write:
```
dotnet ef migrations add [Name]
dotnet ef database update
```

## How To Do Stuff ##
### How to create a new component ###
Be in the 'Client/src/app' directory and write:
```
ng generate component [Name]
```

## The Data ##
Most of the data needed can be found here: [Siberian Husky HTML Gagnagrunnur](http://www.spisshundar.com/siberianhuskyg.htm)

## To Do List ##
Here you can find a Google Sheet with a list of tasks to finish for the project: 
[Google Sheet](https://docs.google.com/spreadsheets/d/1vUVvcqCDZDTPVTs-HkJHXNLFuaGpB7O_rYfJj0iKxMI/edit?usp=sharing)

## Websites To Look At ##
(Both websites show the details for the same dog)

[Pawvillage](https://pawvillage.com/pedigree/dynprofile.asp?ID=KI160KU1H7)

[Ingrus](http://ingrus.net/husky/en/details.php?id=11037)