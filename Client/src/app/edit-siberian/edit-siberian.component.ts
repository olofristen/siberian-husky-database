import { Component, OnInit, Input, SimpleChanges, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SiberianService } from '../siberian.service';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberianViewModel, SiberianSimple, BreederSimple, Countries, SiberianDetail } from '../app.models';
import { AddSiberianComponent } from '../add-siberian/add-siberian.component';

@Component({
  selector: 'app-edit-siberian',
  templateUrl: './edit-siberian.component.html',
  styleUrls: ['./edit-siberian.component.css']
})
export class EditSiberianComponent implements OnInit {

  siberianMales: SiberianSimple[];
  siberianFemales: SiberianSimple[];
  breederList: BreederSimple;
  countryList: Countries;
  newSiberianId: number;
  exists: boolean;
  myForm: FormGroup;
  private sub: any;
  siberianId: number;
  private siberian: SiberianDetail;
  noSiberian: boolean;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal,
    private detect: ChangeDetectorRef,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.exists = false;
    this.noSiberian = true;

    this.myForm = this.fb.group({
      status: new FormControl('', Validators.required),
      titles: new FormControl(''),
      shortTitles: new FormControl(''),
      postTitles: new FormControl(''),
      pedigreeName: new FormControl('', Validators.required),
      callName: new FormControl(),
      dob: new FormControl(),
      gender: new FormControl(),
      microChip: new FormControl(),
      pedigreeNumber: new FormControl(),
      owner: new FormControl(),
      isl_BreederId: new FormControl(),
      breeder: new FormControl(),
      orgCountry: new FormControl(),
      currCountry: new FormControl(),
      hd: new FormControl(),
      color: new FormControl(),
      sire: new FormControl(),
      dam: new FormControl(),
      dead: new FormControl(),
      year: new FormControl(),
      onList: new FormControl()
    });

    this.sub = this.route.params.subscribe(
      params => {
        this.siberianId = +params['id'];
        this.noSiberian = true;

        this.service.getSiberianById(this.siberianId).subscribe(
          currentSiberian => {
            this.siberian = currentSiberian;
            console.log(this.siberian);

            /*if (this.siberian.gender === 'Female') {
              this.siberian.gender = 'Tík';
            }

            if (this.siberian.gender === 'Male') {
              this.siberian.gender = 'Rakki';
            }*/

            this.myForm.setValue({
              status: this.siberian.status,
              titles: this.siberian.titles,
              shortTitles: this.siberian.shortTitles,
              postTitles: this.siberian.postTitles,
              pedigreeName: this.siberian.pedigreeName,
              callName: this.siberian.callName,
              dob: this.siberian.dob,
              gender: this.siberian.gender,
              microChip: this.siberian.microChip,
              pedigreeNumber: this.siberian.pedigreeNumber,
              owner: this.siberian.owners,
              isl_BreederId: '',
              breeder: '',
              orgCountry: this.siberian.orgCountry,
              currCountry: this.siberian.currCountry,
              hd: this.siberian.hd,
              color: this.siberian.color,
              sire: this.siberian.sire.sire.dogPedigreeName,
              dam: this.siberian.dam.dam.dogPedigreeName,
              dead: this.siberian.dead,
              year: this.siberian.year,
              onList: ''
            });

            this.noSiberian = false;
          }, err => {
            this.noSiberian = true;
          });
      });

    this.service.getMales().subscribe(
      males => {
        this.siberianMales = males;
      }, err => {
        console.log('Unable to get the males for search');
      });

    this.service.getFemales().subscribe(
      females => {
        this.siberianFemales = females;
      }, err => {
        console.log('Unable to get the females for search');
      });

    this.service.getBreedersSimple().subscribe(
      breeders => {
        this.breederList = breeders;
      }, err => {
        console.log('Unable to get the breeders for search');
      });

    this.service.getCountries().subscribe(
      countries => {
        this.countryList = countries;
      }, err => {
        console.log('Unable to get the countries for search');
      });
  }


}
