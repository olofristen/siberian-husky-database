import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSiberianComponent } from './edit-siberian.component';

describe('EditSiberianComponent', () => {
  let component: EditSiberianComponent;
  let fixture: ComponentFixture<EditSiberianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSiberianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSiberianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
