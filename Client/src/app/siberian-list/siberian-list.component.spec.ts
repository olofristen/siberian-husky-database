import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiberianListComponent } from './siberian-list.component';

describe('SiberianListComponent', () => {
  let component: SiberianListComponent;
  let fixture: ComponentFixture<SiberianListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiberianListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiberianListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
