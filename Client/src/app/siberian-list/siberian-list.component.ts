import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { SiberianService } from '../siberian.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberianList } from '../app.models';

@Component({
  selector: 'app-siberian-list',
  templateUrl: './siberian-list.component.html',
  styleUrls: ['./siberian-list.component.css']
})

export class SiberianListComponent implements OnInit {

  siberianList: SiberianList[];
  siberianImports: SiberianList[];
  siberianBorn: SiberianList[];
  listsReady: boolean;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.listsReady = false;
    this.service.getSiberians().subscribe(
      siberians => {
        this.siberianList = siberians;
        this.siberianList.sort(this.compareYear); // sort list by years, the most recent first
        console.log(this.siberianList);
        this.siberianList.forEach(year => year.siberianList.sort(this.compareDOB)); // sort dogs by age, youngest first

        this.siberianImports = siberians.filter(
          year => year.siberianList.filter(siberian => siberian.status === 1).length !== 0); // get all imported dogs (status = 1)

        this.siberianBorn = siberians.filter(
          // get all local bred (not export) in Iceland (status = 3)
          year => year.siberianList.filter(siberian => siberian.status === 3).length !== 0);

        this.listsReady = true;
      }, err => {
        console.log('Did not work to get the list of Siberians');
      });
  }

  compareYear(a, b) {
    if (a.year > b.year) {
      return -1;
    }

    if (a.year < b.year) {
      return 1;
    }

    return 0;
  }

  compareDOB(a, b) {
    if (a.dob > b.dob) {
      return -1;
    }

    if (a.dob < b.dob) {
      return 1;
    }

    return 0;
  }
}
