import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SiberianService } from '../siberian.service';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  myForm: FormGroup;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private fb: FormBuilder) { }

  ngOnInit() {

    localStorage.setItem('token', null); // SHOULD NOT BE HERE, FOR DEV PURPOUSE
    this.myForm = this.fb.group({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    });
  }

  submit() {

    const reply = this.service.login(this.myForm.value).subscribe(
      loginInfo => {
        console.log('LOGGED IN!');
        localStorage.setItem('token', loginInfo.token);

        if (loginInfo.role === 'Admin') {
            this.router.navigate(['../admin']);
        } else {
          this.router.navigate(['']);
        }
      }, err => {
        console.log('Unable to log in!');
      }
    );
  }

}
