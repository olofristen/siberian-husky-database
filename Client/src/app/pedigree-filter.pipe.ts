import { Pipe, PipeTransform } from '@angular/core';
import { Siberian } from './app.models';

@Pipe({
  name: 'pedigreeFilter'
})

export class PedigreePipe implements PipeTransform {

    transform(items: Siberian[], searchText: string): any[] {
        if (!items) {
            return [];
        }

        if (!searchText) {
            return items;
        }

        searchText = searchText.toLowerCase();

        return items.filter( it => {
            return it.pedigreeName.toLowerCase().includes(searchText);
        });
   }
}
