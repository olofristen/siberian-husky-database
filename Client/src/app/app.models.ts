  export class SiberianList {
    year: string;
    siberianList: Siberian[];
  }

  export class Siberian {
    id: number;
    status: number;
    titles: string;
    pedigreeName: string;
    dob: string;
    hd: string;
    eye: string;
    dead: boolean;
    breedingStatus: boolean;
    eyeValid: boolean;
  }

  export class SiberianForPedigree {
    dogId: number;
    dogTitles: string;
    dogPedigreeName: string;
    dogHD: string;
    dogEye: string;
    dogColor: string;
    sire: SiberianForPedigree;
    dam: SiberianForPedigree;
  }

  export class EyeResult {
    date: string;
    result: string[];
    comment: string;
    other: string;
  }

  export class SingleShowResult {
    date: string;
    result: string;
  }

  export class SiberianSimple {
    id: number;
    titles: string;
    pedigreeName: string;
  }

  export class LitterSimple {
    sireId: number;
    sirePedName: string;
    damId: number;
    damPedName: string;
    sireTitles: string;
    damTitles: string;
    dob: string;
    progeny: SiberianSimple[];
  }

  export class OwnerList {
    id: number;
    name: string;
    country: string;
  }

  export class OwnerAgreement {
    id: number;
    name: string;
    agreement: boolean;
  }

  export class BreederSimple {
    id: number;
    kennelName: string;
    breederName: string;
    firstLitter: string;
    latestLitter: string;
  }

  export class BreederActive {
    active: BreederSimple[];
    inactive: BreederSimple[];
  }

  export class BreederDetail {
    id: number;
    kennelName: string;
    breederName: string;
    phone: string;
    email: string;
    website: string;
    puppyCount: number;
    owned: SiberianSimple[];
    bred: LitterSimple[];
  }

  export class SiberianDetail {
    id: number;
    status: number;
    titles: string;
    shortTitles: string;
    postTitles: string;
    pedigreeName: string;
    callName: string;
    dob: string;
    gender: string;
    microChip: string;
    pedigreeNumber: string;
    owners: string[];
    isl_Breeder: BreederSimple;
    breeders: string[];
    orgCountry: string;
    currCountry: string;
    hd: string;
    color: string;
    sire: SiberianForPedigree;
    dam: SiberianForPedigree;
    eye: EyeResult[];
    shows: SingleShowResult[];
    dead: boolean;
    litters: LitterSimple[];
    year: string;
  }

  export class BreederViewModel {
    kennelName: string;
    breederName: string;
    phone: string;
    email: string;
    website: string;
  }

  export class SiberianViewModel {
    status: number;
    titles: string;
    shortTitles: string;
    postTitles: string;
    pedigreeName: string;
    callName: string;
    dob: string;
    gender: number;
    microChip: string;
    pedigreeNumber: string;
    owners: OwnerViewModel[];
    breeders: OwnerViewModel[];
    orgCountry: string;
    currCountry: string;
    hd: string;
    color: string;
    sire: number;
    dam: number;
    dead: boolean;
    year: string;
  }

  export class OwnerViewModel {
    name: string;
    country: string;
  }

  export class LitterViewModel {
    breeders: OwnerViewModel[];
    dob: string;
    sire: number;
    dam: number;
    puppies: PuppyViewModel[];
  }

  export class PuppyViewModel {
    pedigreeName: string;
    owners: OwnerViewModel[];
    microChip: string;
    gender: number;
    callName: string;
    pedigreeNumber: string;
    color: string;
  }

  export class EyeExamViewModel {
    date: string;
    siberians: SiberianEyeViewModel[];
  }

  export class SiberianEyeViewModel {
    siberianId: number;
    comment: string;
    other: string;
    results: number[];
  }

  export class AgreementChangeViewModel {
    id: number;
    agreement: boolean;
  }

  export class Countries {
    countryCode: string;
    country: string;
  }

  export class LoginViewModel {
    email: string;
    password: string;
    rememberMe: boolean;
}

export class LoginDTO {
    role: string;
    token: string;
}
