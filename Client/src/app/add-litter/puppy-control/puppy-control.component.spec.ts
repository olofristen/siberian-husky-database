import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuppyControlComponent } from './puppy-control.component';

describe('PuppyControlComponent', () => {
  let component: PuppyControlComponent;
  let fixture: ComponentFixture<PuppyControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuppyControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuppyControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
