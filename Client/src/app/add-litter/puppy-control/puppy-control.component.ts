import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SiberianService} from '../../siberian.service';
import { PuppiesArrayComponent } from '../puppies-array/puppies-array.component';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OwnersArrayComponent } from '../../owners-array/owners-array.component';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'puppy-control',
  templateUrl: './puppy-control.component.html',
  styleUrls: ['./puppy-control.component.css']
})
export class PuppyControlComponent implements OnInit {

  @Input()
  public index: number;

  @Input()
  public puppy: FormGroup;

  @Output()
  public removed: EventEmitter<number> = new EventEmitter<number>();

  static buildPuppy() {
    return new FormGroup({
      pedigreeName: new FormControl('', Validators.required),
      microChip: new FormControl(''),
      owners: OwnersArrayComponent.buildOwners(),
      gender: new FormControl('', Validators.required),
      callName: new FormControl('', Validators.required),
      pedigreeNumber: new FormControl('', Validators.required),
      color: new FormControl('')
    });
  }

  constructor(private service: SiberianService) { }

  ngOnInit() {}

}
