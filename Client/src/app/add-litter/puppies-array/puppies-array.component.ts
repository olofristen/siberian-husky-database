import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SiberianService } from '../../siberian.service';
import { PuppyControlComponent } from '../puppy-control/puppy-control.component';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'puppies-array',
  templateUrl: './puppies-array.component.html',
  styleUrls: ['./puppies-array.component.css']
})
export class PuppiesArrayComponent implements OnInit {

  @Input()
  public puppiesFormArray: FormArray;

  static buildPuppies() {
    return new FormArray([
      PuppyControlComponent.buildPuppy(),
    ]);
  }

  constructor() { }

  ngOnInit() {
  }

  addPuppy() {
    this.puppiesFormArray.push(PuppyControlComponent.buildPuppy());
  }
}
