import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuppiesArrayComponent } from './puppies-array.component';

describe('PuppiesArrayComponent', () => {
  let component: PuppiesArrayComponent;
  let fixture: ComponentFixture<PuppiesArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuppiesArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuppiesArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
