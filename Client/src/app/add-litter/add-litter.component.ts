import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SiberianService } from '../siberian.service';
import { PuppiesArrayComponent } from './puppies-array/puppies-array.component';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberianSimple, BreederSimple, OwnerList } from '../app.models';
import { OwnersArrayComponent } from '../owners-array/owners-array.component';

@Component({
  selector: 'app-add-litter',
  templateUrl: './add-litter.component.html',
  styleUrls: ['./add-litter.component.css']
})

export class AddLitterComponent implements OnInit {

  siberianMales: SiberianSimple[];
  siberianFemales: SiberianSimple[];
  breederList: BreederSimple;
  ownersList: OwnerList[];
  myForm: FormGroup;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal,
    private fb: FormBuilder) { }

  ngOnInit() {

    this.service.getMales().subscribe(
      males => {
        this.siberianMales = males;
      }, err => {
        console.log('Unable to get the males for search');
      });

    this.service.getFemales().subscribe(
      females => {
        this.siberianFemales = females;
      }, err => {
        console.log('Unable to get the females for search');
      });

    this.service.getBreedersSimple().subscribe(
      breeders => {
        this.breederList = breeders;
      }, err => {
        console.log('Unable to get the breeders for search');
      });

    this.service.getOwnerList().subscribe(
      owners => {
        this.ownersList = owners;
      }, err => {
        console.log('Unable to get owners for search');
      });

    this.myForm = this.fb.group({
      breeders: OwnersArrayComponent.buildOwners(),
      dob: new FormControl('', Validators.required),
      sire: new FormControl('', Validators.required),
      dam: new FormControl('', Validators.required),
      puppies: PuppiesArrayComponent.buildPuppies()
    });
  }

  submit() {
    console.log(this.myForm.value);

    const reply = this.service.addLitter(this.myForm.value).subscribe(
      litter => {
        this.router.navigate(['../db/']);
      }, err => {
        console.log('Something went wrong with adding the new siberian');
      });
  }

}
