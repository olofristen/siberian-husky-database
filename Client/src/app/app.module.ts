import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { SiberianService } from './siberian.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SiberianListComponent } from './siberian-list/siberian-list.component';
import { SiberianDetailComponent } from './siberian-detail/siberian-detail.component';
import { AddSiberianComponent } from './add-siberian/add-siberian.component';
import { AddLitterComponent } from './add-litter/add-litter.component';
import { AddBreederComponent } from './add-breeder/add-breeder.component';
import { BreederListComponent } from './breeder-list/breeder-list.component';
import { BreederDetailComponent } from './breeder-detail/breeder-detail.component';
import { PuppiesArrayComponent } from './add-litter/puppies-array/puppies-array.component';
import { PuppyControlComponent } from './add-litter/puppy-control/puppy-control.component';
import { SiberianHuskyInformationComponent } from './siberian-husky-information/siberian-husky-information.component';
import { AddEyeResultsComponent } from './add-eye-results/add-eye-results.component';
import { SiberiansArrayComponent } from './add-eye-results/siberians-array/siberians-array.component';
import { SiberianControlComponent } from './add-eye-results/siberian-control/siberian-control.component';
import { LoginComponent } from './login/login.component';
import { RoleService as Role, RoleService } from './AuthenticationServices/role.service';
import { AuthGuardService } from './AuthenticationServices/auth-guard.service';
import { AuthService } from './AuthenticationServices/auth.service';
import { JwtHelper } from './AuthenticationServices/jwt-helper.service';
import { AdminComponent } from './admin/admin.component';
import { EditSiberianComponent } from './edit-siberian/edit-siberian.component';
import { PedigreePipe } from './pedigree-filter.pipe';
import { OwnerPipe } from './name-filter.pipe';
import { AdminActionsComponent } from './admin/admin-actions/admin-actions.component';
import { EditOwnerAgreementsComponent } from './edit-owner-agreements/edit-owner-agreements.component';
import { OwnerControlComponent } from './owner-control/owner-control.component';
import { OwnersArrayComponent } from './owners-array/owners-array.component';

@NgModule({
  declarations: [
    AppComponent,
    SiberianListComponent,
    SiberianDetailComponent,
    AddSiberianComponent,
    AddLitterComponent,
    AddBreederComponent,
    BreederListComponent,
    BreederDetailComponent,
    PuppiesArrayComponent,
    PuppyControlComponent,
    SiberianHuskyInformationComponent,
    AddEyeResultsComponent,
    SiberiansArrayComponent,
    SiberianControlComponent,
    AdminComponent,
    LoginComponent,
    EditSiberianComponent,
    PedigreePipe,
    OwnerPipe,
    AdminActionsComponent,
    EditOwnerAgreementsComponent,
    OwnerControlComponent,
    OwnersArrayComponent
    ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([{
      path: '',
      redirectTo: '',
      pathMatch: 'full'
      }, {
        path: 'siberian-husky',
        component: SiberianHuskyInformationComponent
      }, {
        path: 'breeders',
        component: BreederListComponent
      }, {
        path: 'breeders/:id',
        component: BreederDetailComponent
      }, {
        path: 'db',
        component: SiberianListComponent
      }, {
        path: 'db/addSiberian',
        component: AddSiberianComponent,
        canActivate: [Role],
        data: {
          expectedRole: 'Admin'
        }
      }, {
        path: 'db/addLitter',
        component: AddLitterComponent,
        canActivate: [Role],
        data: {
          expectedRole: 'Admin'
        }
      }, {
        path: 'db/addBreeder',
        component: AddBreederComponent,
        canActivate: [Role],
        data: {
          expectedRole: 'Admin'
        }
      }, {
        path: 'db/addEyeResults',
        component: AddEyeResultsComponent,
        canActivate: [Role],
        data: {
          expectedRole: 'Admin'
        }
      }, {
        path: 'db/editOwnerAgreements',
        component: EditOwnerAgreementsComponent,
        canActivate: [Role],
        data: {
          expectedRole: 'Admin'
        }
      }, {
        path: 'db/:id',
        component: SiberianDetailComponent
      }, {
        path: 'login',
        component: LoginComponent
      }, {
        path: 'admin',
        component: AdminComponent,
        canActivate: [Role],
        data: {
          expectedRole: 'Admin'
        }
      }, {
        path: 'db/editSiberian/:id',
        component: EditSiberianComponent
      }])
    ],
  providers: [
    SiberianService,
    AuthGuardService,
    AuthService,
    RoleService,
    JwtHelper
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
