import { TestBed, inject } from '@angular/core/testing';

import { SiberianService } from './siberian.service';

describe('SiberianService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiberianService]
    });
  });

  it('should be created', inject([SiberianService], (service: SiberianService) => {
    expect(service).toBeTruthy();
  }));
});
