import { Pipe, PipeTransform } from '@angular/core';
import { Siberian, OwnerAgreement } from './app.models';

@Pipe({
  name: 'ownerFilter'
})

export class OwnerPipe implements PipeTransform {

    transform(items: OwnerAgreement[], searchText: string): any[] {
        if (!items) {
            return [];
        }

        if (!searchText) {
            return items;
        }

        searchText = searchText.toLowerCase();

        return items.filter( it => {
            return it.name.toLowerCase().includes(searchText);
        });
   }
}
