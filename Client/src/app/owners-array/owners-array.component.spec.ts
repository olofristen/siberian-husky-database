import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnersArrayComponent } from './owners-array.component';

describe('OwnersArrayComponent', () => {
  let component: OwnersArrayComponent;
  let fixture: ComponentFixture<OwnersArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnersArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnersArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
