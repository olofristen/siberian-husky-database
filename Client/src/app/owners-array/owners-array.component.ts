import { Component, OnInit, Input } from '@angular/core';
import { OwnerControlComponent } from '../owner-control/owner-control.component';
import { FormArray } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'owners-array',
  templateUrl: './owners-array.component.html',
  styleUrls: ['./owners-array.component.css']
})
export class OwnersArrayComponent implements OnInit {

  @Input()
  public ownersFormArray: FormArray;

  static buildOwners() {
    return new FormArray([
      OwnerControlComponent.buildOwner(),
    ]);
  }

  constructor() { }

  ngOnInit() {}

  addOwner() {
    this.ownersFormArray.push(OwnerControlComponent.buildOwner());
  }

}
