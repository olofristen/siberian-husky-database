import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { SiberianService } from '../siberian.service';
import { OwnerAgreement, AgreementChangeViewModel } from '../app.models';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { OwnerPipe } from '../name-filter.pipe';

@Component({
  selector: 'app-edit-owner-agreements',
  templateUrl: './edit-owner-agreements.component.html',
  styleUrls: ['./edit-owner-agreements.component.css']
})
export class EditOwnerAgreementsComponent implements OnInit {

  private owners: OwnerAgreement[];
  private searchOwners: OwnerAgreement[];
  private agreementChange: AgreementChangeViewModel[];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.service.getOwnerAgreements().subscribe(
      owners => {
        this.owners = owners;
        this.searchOwners = owners;
        console.log(this.owners);
      }, err => {
        console.log('Was unable to get the list of owners');
      });

    this.agreementChange = [];
  }

  addToList(id: number, value: boolean) {
    const arrIndex = this.agreementChange.findIndex(owner => owner.id === id);
    console.log(arrIndex);

    if (arrIndex > -1) {
      this.agreementChange.splice(arrIndex, 1);
    }

    if (arrIndex === -1) {
      const index = this.searchOwners.findIndex(owner => owner.id === id);
      this.searchOwners[index].agreement = !value;
      const pack: AgreementChangeViewModel = new AgreementChangeViewModel();
      pack.id = id;
      pack.agreement = !value;
      this.agreementChange.push(pack);
    }

    console.log(this.agreementChange);
  }

  saveOwnerAgreement() {
    const reply = this.service.updateOwnerAgreement(this.agreementChange).subscribe(
      agreement => {
        this.router.navigate(['../admin']);
      }, err => {
        console.log('Something went wrong with saving the owner agreements');
      });
  }
}
