import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOwnerAgreementsComponent } from './edit-owner-agreements.component';

describe('EditOwnerAgreementsComponent', () => {
  let component: EditOwnerAgreementsComponent;
  let fixture: ComponentFixture<EditOwnerAgreementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditOwnerAgreementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOwnerAgreementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
