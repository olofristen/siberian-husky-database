import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { SiberianService } from '../siberian.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BreederActive } from '../app.models';

@Component({
  selector: 'app-breeder-list',
  templateUrl: './breeder-list.component.html',
  styleUrls: ['./breeder-list.component.css']
})
export class BreederListComponent implements OnInit {

  breederList: BreederActive;
  noBreederList: boolean;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.noBreederList = false;
    this.service.getBreeders().subscribe(
      breeders => {
        this.breederList = breeders;
        this.noBreederList = true;
      }, err => {
        console.log('Did not work to get the list of breeders');
      });
  }

}
