import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreederListComponent } from './breeder-list.component';

describe('BreederListComponent', () => {
  let component: BreederListComponent;
  let fixture: ComponentFixture<BreederListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreederListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreederListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
