import { Component, OnInit } from '@angular/core';
import { SiberianService } from '../siberian.service';
import { NgForm } from '@angular/forms';
import { BreederViewModel } from '../app.models';

@Component({
  selector: 'app-add-breeder',
  templateUrl: './add-breeder.component.html',
  styleUrls: ['./add-breeder.component.css']
})

export class AddBreederComponent implements OnInit {

  newBreeder: BreederViewModel;

  constructor(private service: SiberianService) { }

  ngOnInit() {

  }

  onSubmit(s: NgForm) {
    this.newBreeder = s.value;
    console.log(this.newBreeder);
    this.service.addBreeder(this.newBreeder);
  }

}
