import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBreederComponent } from './add-breeder.component';

describe('AddBreederComponent', () => {
  let component: AddBreederComponent;
  let fixture: ComponentFixture<AddBreederComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBreederComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBreederComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
