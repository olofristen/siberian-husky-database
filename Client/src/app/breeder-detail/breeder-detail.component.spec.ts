import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreederDetailComponent } from './breeder-detail.component';

describe('BreederDetailComponent', () => {
  let component: BreederDetailComponent;
  let fixture: ComponentFixture<BreederDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreederDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreederDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
