import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { SiberianService } from '../siberian.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BreederDetail } from '../app.models';

@Component({
  selector: 'app-breeder-detail',
  templateUrl: './breeder-detail.component.html',
  styleUrls: ['./breeder-detail.component.css']
})
export class BreederDetailComponent implements OnInit {

  breederId: number;
  private breeder: BreederDetail;
  noBreeder: boolean;
  private sub: any;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.noBreeder = true;
    this.sub = this.route.params.subscribe(
      params => {
        this.breederId = +params['id'];
        this.noBreeder = true;

        this.service.getBreederById(this.breederId).subscribe(
          currentBreeder => {
            this.breeder = currentBreeder;
            this.breeder.bred.sort(this.compareDOB); // sort litters by age, youngest first
            console.log(this.breeder);
            this.noBreeder = false;
          }, err => {
            this.noBreeder = true;
          });
      });
  }

  compareDOB(a, b) {
    
    if (a.dob > b.dob)
      return -1;
    if (a.dob < b.dob)
      return 1;
    return 0;
  }

}
