import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSiberianComponent } from './add-siberian.component';

describe('AddSiberianComponent', () => {
  let component: AddSiberianComponent;
  let fixture: ComponentFixture<AddSiberianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSiberianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSiberianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
