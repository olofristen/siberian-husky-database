import { Component, OnInit, Input, SimpleChanges, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SiberianService } from '../siberian.service';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberianSimple, BreederSimple, Countries, SiberianDetail } from '../app.models';
import { OwnersArrayComponent } from '../owners-array/owners-array.component';

@Component({
  selector: 'app-add-siberian',
  templateUrl: './add-siberian.component.html',
  styleUrls: ['./add-siberian.component.css']
})

export class AddSiberianComponent implements OnInit {

  siberianMales: SiberianSimple[];
  siberianFemales: SiberianSimple[];
  siberian: SiberianDetail;
  breederList: BreederSimple;
  countryList: Countries;
  newSiberianId: number;
  exists: boolean;
  myForm: FormGroup;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal,
    private detect: ChangeDetectorRef,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.exists = false;

    this.service.getMales().subscribe(
      males => {
        this.siberianMales = males;
      }, err => {
        console.log('Unable to get the males for search');
      });

    this.service.getFemales().subscribe(
      females => {
        this.siberianFemales = females;
      }, err => {
        console.log('Unable to get the females for search');
      });

    this.service.getBreedersSimple().subscribe(
      breeders => {
        this.breederList = breeders;
      }, err => {
        console.log('Unable to get the breeders for search');
      });

    this.service.getCountries().subscribe(
      countries => {
        this.countryList = countries;
      }, err => {
        console.log('Unable to get the countries for search');
      });

    this.myForm = this.fb.group({
      status: new FormControl('', Validators.required),
      titles: new FormControl(''),
      shortTitles: new FormControl(''),
      postTitles: new FormControl(''),
      pedigreeName: new FormControl('', Validators.required),
      callName: new FormControl(''),
      dob: new FormControl(''),
      gender: new FormControl(''),
      microChip: new FormControl(''),
      pedigreeNumber: new FormControl(''),
      owners: OwnersArrayComponent.buildOwners(),
      isl_BreederId: new FormControl(0),
      breeders: OwnersArrayComponent.buildOwners(),
      orgCountry: new FormControl(''),
      currCountry: new FormControl(''),
      hd: new FormControl(''),
      color: new FormControl(''),
      sire: new FormControl(0),
      dam: new FormControl(0),
      dead: new FormControl(false),
      year: new FormControl(''),
    });
  }

  check() {
    this.exists = false;

    for (const sibe of this.siberianMales) {
      if (this.myForm.get('pedigreeName').value.toLowerCase()  === sibe.pedigreeName.toLowerCase()) {
        this.exists = true;
      }
    }

    if (!this.exists) {
      for (const sibe of this.siberianFemales) {
        if (this.myForm.get('pedigreeName').value.toLowerCase()  === sibe.pedigreeName.toLowerCase()) {
          this.exists = true;
        }
      }
    }
  }

  submit() {
    console.log(this.myForm.value);

    const reply = this.service.addSiberian(this.myForm.value).subscribe(
      siberian => {
        this.newSiberianId = siberian.id;
        this.router.navigate(['../db/', this.newSiberianId]);
      }, err => {
        console.log('Something went wrong with adding the new siberian');
      });
  }
}
