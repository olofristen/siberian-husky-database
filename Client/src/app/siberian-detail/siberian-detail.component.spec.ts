import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiberiandetailComponent } from './siberiandetail.component';

describe('SiberiandetailComponent', () => {
  let component: SiberiandetailComponent;
  let fixture: ComponentFixture<SiberiandetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiberiandetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiberiandetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
