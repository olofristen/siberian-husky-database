import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { SiberianService } from '../siberian.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberianDetail } from '../app.models';

@Component({
  selector: 'app-siberiandetail',
  templateUrl: './siberian-detail.component.html',
  styleUrls: ['./siberian-detail.component.css']
})
export class SiberianDetailComponent implements OnInit {

  siberianId: number;
  private siberian: SiberianDetail;
  noSiberian: boolean;
  private sub: any;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal) { }

  ngOnInit() {
    this.noSiberian = true;
    this.sub = this.route.params.subscribe(
      params => {
        this.siberianId = +params['id'];
        this.noSiberian = true;

        this.service.getSiberianById(this.siberianId).subscribe(
          currentSiberian => {
            this.siberian = currentSiberian;
            this.siberian.litters.sort(this.compareDOB);
            this.noSiberian = false;
            console.log(this.siberian);
          }, err => {
            this.noSiberian = true;
          });
      });
  }

  compareDOB(a, b) {
    if (a.dob > b.dob) {
      return -1;
    }
    if (a.dob < b.dob) {
      return 1;
    }

    return 0;
  }

}
