import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/rx';
import { BreederActive, BreederSimple, SiberianDetail, SiberianSimple, BreederDetail, AgreementChangeViewModel } from './app.models';
import { BreederViewModel, LitterViewModel, EyeExamViewModel, LoginViewModel, LoginDTO, Countries, SiberianViewModel } from './app.models';
import { OwnerAgreement, OwnerList, SiberianList, Siberian } from './app.models';

@Injectable()
export class SiberianService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  /*private handleError (error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }*/

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  getOwnerList(): Observable<OwnerList[]> {
    return this.http.get('/api/owners/')
      .map(response => {
        return<OwnerList[]>response;
    });
  }

  getOwnerAgreements(): Observable<OwnerAgreement[]> {
    return this.http.get('/api/owners/agreement')
      .map(response => {
        return <OwnerAgreement[]>response;
    });
  }

  getSiberians(): Observable<SiberianList[]> {
    return this.http.get<SiberianList[]>('/api/siberians')
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }

  getBreeders(): Observable<BreederActive> {
    return this.http.get('/api/breeders')
      .map(response => {
        return<BreederActive>response;
    });
  }

  getBreedersSimple(): Observable<BreederSimple> {
    return this.http.get('/api/breeders/simple')
      .map(response => {
        return<BreederSimple>response;
    });
  }

  getSiberianById(id: number): Observable<SiberianDetail> {
    return this.http.get('/api/siberians/' + id)
      .map(response => {
        return<SiberianDetail>response;
    });
  }

  getIcelandicSiberians(): Observable<SiberianSimple> {
    return this.http.get('/api/siberians/is')
      .map(response => {
        return<SiberianSimple>response;
    });
  }

  getMales(): Observable<SiberianSimple[]> {
    return this.http.get('/api/siberians/males')
      .map(response => {
        return<SiberianSimple[]>response;
    });
  }

  getFemales(): Observable<SiberianSimple[]> {
    return this.http.get('/api/siberians/females')
      .map(response => {
        return<SiberianSimple[]>response;
    });
  }

  getAllSiberians(): Observable<Siberian[]> {
    return this.http.get('api/siberians/all')
      .map(response => {
        return<Siberian[]>response;
      });
  }

  getBreederById(id: number): Observable<BreederDetail> {
    return this.http.get('api/breeders/' + id)
      .map(response => {
        return<BreederDetail>response;
    });
  }

  getCountries(): Observable<Countries> {
    return this.http.get('api/other/country')
      .map(response => {
        return<Countries>response;
    });
  }

  addSiberian(newSiberian: SiberianViewModel): Observable<SiberianDetail> {
    console.log(newSiberian);

    return this.http.post<SiberianDetail>('/api/siberians', newSiberian, this.httpOptions)
      .pipe(
        catchError(this.handleError)
    );
  }

  addLitter(newLitter: LitterViewModel): Observable<number> {
    console.log(newLitter);

    return this.http.post<number>('/api/siberians/litter', newLitter, this.httpOptions)
      .pipe(
        catchError(this.handleError)
    );
  }

  addBreeder(newBreeder: BreederViewModel): Observable<BreederViewModel> {
    console.log(newBreeder.kennelName);

    return this.http.post<BreederViewModel>('/api/breeders', newBreeder, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  addEyeExam(newEyeExam: EyeExamViewModel): Observable<number> {
    console.log(newEyeExam);

    return this.http.post<number>('/api/eyeexam', newEyeExam, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  updateOwnerAgreement(updatedAgreement: AgreementChangeViewModel[]): Observable<boolean> {
    console.log(updatedAgreement);

    return this.http.put<boolean>('/api/owners/agreement', updatedAgreement, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  login(loginInfo: LoginViewModel): Observable<LoginDTO> {
    console.log('logging in');

    return this.http.post<LoginDTO>('api/account/login', loginInfo, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
}
