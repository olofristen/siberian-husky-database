import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OwnerList } from '../app.models';
import { SiberianService } from '../siberian.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'owner-control',
  templateUrl: './owner-control.component.html',
  styleUrls: ['./owner-control.component.css']
})
export class OwnerControlComponent implements OnInit {

  ownerList: OwnerList[];

  @Input()
  public index: number;

  @Input()
  public owner: FormGroup;

  @Output()
  public removed: EventEmitter<number> = new EventEmitter<number>();

  static buildOwner() {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      country: new FormControl('')
    });
  }

  constructor(private service: SiberianService) { }

  ngOnInit() {
    this.service.getOwnerList().subscribe(
      owners => {
        this.ownerList = owners;
      }, err => {
        console.log('Unable to get owners for search');
      });
  }
}
