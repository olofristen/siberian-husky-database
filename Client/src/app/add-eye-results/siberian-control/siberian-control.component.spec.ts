import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiberianControlComponent } from './siberian-control.component';

describe('SiberianControlComponent', () => {
  let component: SiberianControlComponent;
  let fixture: ComponentFixture<SiberianControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiberianControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiberianControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
