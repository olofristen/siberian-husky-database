import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SiberianService } from '../../siberian.service';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberianSimple } from '../../app.models';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'siberian-control',
  templateUrl: './siberian-control.component.html',
  styleUrls: ['./siberian-control.component.css']
})
export class SiberianControlComponent implements OnInit {

  siberianList: SiberianSimple;
  siberiansReady: boolean;
  // tslint:disable-next-line:member-ordering
  static form: FormGroup;

  constructor(private service: SiberianService) { }

  ngOnInit() {

    this.siberiansReady = false;

    this.service.getIcelandicSiberians().subscribe(
      siberians => {
        this.siberianList = siberians;
        this.siberiansReady = true;
      }, err => {
        console.log('Unable to get the siberians for search');
      });
  }

  // tslint:disable-next-line:member-ordering
  @Input()
  public index: number;

  // tslint:disable-next-line:member-ordering
  @Input()
  public siberian: FormGroup;

  // tslint:disable-next-line:member-ordering
  @Output()
  public removed: EventEmitter<number> = new EventEmitter<number>();

  // tslint:disable-next-line:member-ordering
  static buildSiberian() {
    this.form = new FormGroup({
      siberianId: new FormControl('', Validators.required),
      comment: new FormControl(''),
      other: new FormControl(''),
      results: new FormArray([])
    });

    return this.form;
  }

  middler(result: string, isChecked: boolean) {
    SiberianControlComponent.onChange(result, isChecked);
  }

  // tslint:disable-next-line:member-ordering
  static onChange(result: string, isChecked: boolean) {
    const resultFormArray = <FormArray>this.form.controls.results;

    if (isChecked) {
      resultFormArray.push(new FormControl(result));
    } else {
      const index = resultFormArray.controls.findIndex(x => x.value === result);
      resultFormArray.removeAt(index);
    }
  }
}
