import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SiberianService } from '../siberian.service';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SiberiansArrayComponent } from './siberians-array/siberians-array.component';
import { SiberianSimple } from '../app.models';

@Component({
  selector: 'app-add-eye-results',
  templateUrl: './add-eye-results.component.html',
  styleUrls: ['./add-eye-results.component.css']
})
export class AddEyeResultsComponent implements OnInit {

  siberians: SiberianSimple;
  myForm: FormGroup;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private service: SiberianService,
    private modalService: NgbModal,
    private fb: FormBuilder) { }

  ngOnInit() {

    this.myForm = this.fb.group({
      date: new FormControl('', Validators.required),
      siberians: SiberiansArrayComponent.buildSiberians()
    });
  }

  submit() {
    console.log(this.myForm.value);

    const reply = this.service.addEyeExam(this.myForm.value).subscribe(
      litter => {
        this.router.navigate(['../db/']);
      }, err => {
        console.log('Something went wrong with adding the new siberian');
      });
  }

}
