import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SiberianService } from '../../siberian.service';
import { SiberianControlComponent } from '../siberian-control/siberian-control.component';
import { Router, ActivatedRoute, RouterLink } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'siberians-array',
  templateUrl: './siberians-array.component.html',
  styleUrls: ['./siberians-array.component.css']
})
export class SiberiansArrayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  // tslint:disable-next-line:member-ordering
  @Input()
  public siberiansFormArray: FormArray;

  addSiberian() {
    this.siberiansFormArray.push(SiberianControlComponent.buildSiberian());
  }

  // tslint:disable-next-line:member-ordering
  static buildSiberians() {
    return new FormArray([
      SiberianControlComponent.buildSiberian(),
    ]);
  }

}
