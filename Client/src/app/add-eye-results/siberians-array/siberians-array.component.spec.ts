import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiberiansArrayComponent } from './siberians-array.component';

describe('SiberiansArrayComponent', () => {
  let component: SiberiansArrayComponent;
  let fixture: ComponentFixture<SiberiansArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiberiansArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiberiansArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
