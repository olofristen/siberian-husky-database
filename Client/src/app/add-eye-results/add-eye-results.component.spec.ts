import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEyeResultsComponent } from './add-eye-results.component';

describe('AddEyeResultsComponent', () => {
  let component: AddEyeResultsComponent;
  let fixture: ComponentFixture<AddEyeResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEyeResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEyeResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
