import { Component, OnInit } from '@angular/core';
import { Siberian } from '../app.models';
import { SiberianService } from '../siberian.service';
import { NgModule } from '@angular/core';
import { NgForm, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PedigreePipe} from '../pedigree-filter.pipe';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  listReady: boolean;
  allSiberians: Siberian[];
  searchSiberians: Siberian[];

  constructor(private service: SiberianService,
    private modalService: NgbModal,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.listReady = false;

    this.service.getAllSiberians().subscribe(
      siberians => {
        this.allSiberians = siberians;
        this.searchSiberians = siberians;
        console.log(this.searchSiberians);
        this.listReady = true;
      }, err => {
        console.log('Unable to get list of Siberians');
        this.listReady = false;
      });
  }
}
