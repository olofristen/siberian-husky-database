import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiberianHuskyInformationComponent } from './siberian-husky-information.component';

describe('SiberianHuskyInformationComponent', () => {
  let component: SiberianHuskyInformationComponent;
  let fixture: ComponentFixture<SiberianHuskyInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiberianHuskyInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiberianHuskyInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
