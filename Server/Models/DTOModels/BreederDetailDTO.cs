using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class BreederDetailDTO
    {
        public int ID { get; set; }
        public List<string> breederName { get; set; }
        public string kennelName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public int puppyCount { get; set; }
        public IEnumerable<SiberianSimpleDTO> owned { get; set; }
        public IEnumerable<LitterSimpleDTO> bred { get; set; }
    }
}