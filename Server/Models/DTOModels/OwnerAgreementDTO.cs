namespace SHDatabase.Models.DTOModels
{
    public class OwnerAgreementDTO
    {
        public int ID { get; set; }
        public string name { get; set; }
        public bool agreement { get; set; }
    }
}