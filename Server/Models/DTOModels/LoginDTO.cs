namespace SHDatabase.Models.DTOModels
{
    public class LoginDTO
    {
        public string Role { get; set; }

        public string Token { get; set; }
    }
}