using System;

namespace SHDatabase.Models.DTOModels
{
    public class SiberianDTO
    {
        public int ID { get; set; }
        public int Status { get; set; }
        public string Titles { get; set; }
        public string PedigreeName { get; set; }
        public string DOB { get; set; }
        public string HD { get; set; }
        public string Eye { get; set; }
        public bool Dead { get; set; }
        public string Year { get; set; }
        public bool BreedingStatus { get; set; }
        public bool EyeValid { get; set; }
    }
}