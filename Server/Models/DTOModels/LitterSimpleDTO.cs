using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class LitterSimpleDTO
    {
        public int SireID { get; set; }
        public string SirePedName { get; set; }
        public int DamID { get; set; }
        public string DamPedName { get; set; }
        public string SireTitles { get; set; }
        public string DamTitles { get; set; }
        public string DOB { get; set; }
        public List<SiberianSimpleDTO> Progeny { get; set; }
    }
}