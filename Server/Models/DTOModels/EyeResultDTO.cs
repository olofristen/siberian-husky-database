using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class EyeResultDTO
    {
        public string Date { get; set; }
        public IEnumerable<string> result { get; set; }
        public string comment { get; set; }
        public string other { get; set; }
    }
}