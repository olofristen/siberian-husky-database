using System;

namespace SHDatabase.Models.DTOModels
{
    public class SingleShowResultDTO
    {
        public DateTime Date { get; set; }
        public string Result { get; set; }
    }
}