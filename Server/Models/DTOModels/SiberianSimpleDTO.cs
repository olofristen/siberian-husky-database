using System;

namespace SHDatabase.Models.DTOModels
{
    public class SiberianSimpleDTO
    {
        public int ID { get; set; }
        public string Titles { get; set; }
        public string PedigreeName { get; set; }
    }
}