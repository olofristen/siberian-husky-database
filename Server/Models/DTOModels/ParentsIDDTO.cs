using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class ParentsIDDTO
    {
        public int DogID { get; set; }
        public int SireID { get; set; }
        public int DamID { get; set; }
    }
}