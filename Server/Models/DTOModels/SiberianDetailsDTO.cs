using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class SiberianDetailsDTO
    {
        public int ID { get; set; }
        public int Status { get; set; }
        public string Titles { get; set; }
        public string ShortTitles { get; set; }
        public string PostTitles { get; set; }
        public string PedigreeName { get; set; }
        public string CallName { get; set; }
        public string DOB { get; set; }
        public string MicroChip { get; set; }
        public string Gender { get; set; }
        public string PedigreeNumber { get; set; }
        public List<string> Owners { get; set; }
        public List<string> Breeders { get; set; }
        public string OrgCountry { get; set; }
        public string CurrCountry { get; set; }
        public string Color { get; set; }
        public string HD { get; set; }
        public string Year { get; set; }
        public SiberianForPedigreeDTO Sire { get; set; }
        public SiberianForPedigreeDTO Dam { get; set;}
        public IEnumerable<EyeResultDTO> Eye { get; set; }
        public bool BreedingStatus { get; set; }
        public IEnumerable<SingleShowResultDTO> Shows { get; set; }
        public bool Dead { get; set; }
        public IEnumerable<LitterSimpleDTO> Litters { get; set; }
    }
}