using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class BreederListDTO
    {
        public List<BreederSimpleDTO> active { get; set; }
        public List<BreederSimpleDTO> inactive { get; set; }
    }
}