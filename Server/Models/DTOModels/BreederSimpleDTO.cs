using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class BreederSimpleDTO
    {
        public int ID { get; set; }
        public List<string> breederName { get; set; }
        public string kennelName { get; set; }
        public string firstLitter { get; set; }
        public string latestLitter { get; set; }
        public bool active { get; set; }
    }
}