using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class SiberianListDTO
    {
        public string year { get; set; }
        public List<SiberianDTO> siberianList { get; set; }
    }
}