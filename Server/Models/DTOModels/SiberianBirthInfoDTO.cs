using System;

namespace SHDatabase.Models.DTOModels
{
    public class SiberianBirthInfoDTO
    {
        public int ID { get; set; }
        public string PedName { get; set; }
        public string Titles { get; set; }
        public string DOB { get; set; }
        public int SireID { get; set; }
        public string SirePedName { get; set; }
        public int DamID { get; set; }
        public string DamPedName { get; set; }
        public string SireTitles { get; set; }
        public string DamTitles { get; set; }
    }
}