namespace SHDatabase.Models.DTOModels
{
    public class SiberianForPedigreeDTO
    {
        public int DogID { get; set; }
        public string DogTitles { get; set; }
        public string DogPedigreeName { get; set; }
        public string DogHD { get; set; }
        public string DogEye { get; set; }
        public string DogColor { get; set; }
        public SiberianForPedigreeDTO Sire { get; set; }
        public SiberianForPedigreeDTO Dam { get; set; }
    }
}