using System;
using System.Collections.Generic;

namespace SHDatabase.Models.DTOModels
{
    public class CountryDTO
    {
        public string countryCode { get; set; }
        public string country { get; set; }
    }
}