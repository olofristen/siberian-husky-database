namespace SHDatabase.Models.DTOModels
{
    public class OwnerListDTO
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string country { get; set; }
    }
}