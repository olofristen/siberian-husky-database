namespace SHDatabase.Models.EntityModels
{
    public class Parents
    {
        public int dogID { get; set; }
        public int sireID { get; set; }
        public int damID { get; set; }
    }
}