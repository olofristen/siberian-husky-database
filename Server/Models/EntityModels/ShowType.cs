namespace SHDatabase.Models.EntityModels
{
    public class ShowType
    {
        public int type { get; set; }
        public string description { get; set; }
    }
}