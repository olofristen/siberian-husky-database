namespace SHDatabase.Models.EntityModels
{
    public class Owner
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string country { get; set; }
        public bool agreement { get; set; }
    }
}