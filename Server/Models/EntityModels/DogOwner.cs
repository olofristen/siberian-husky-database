namespace SHDatabase.Models.EntityModels
{
    public class DogOwner
    {
        public int dogID { get; set; }
        public int ownerID { get; set; }
    }
}