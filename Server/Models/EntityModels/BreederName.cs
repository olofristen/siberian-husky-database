namespace SHDatabase.Models.EntityModels
{
    public class BreederName
    {
        public int kennelID { get; set; }
        public int ownerID { get; set; }
    }
}