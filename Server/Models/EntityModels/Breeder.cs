namespace SHDatabase.Models.EntityModels
{
    public class Kennel
    {
        public int ID { get; set; }
        public string kennelName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string website { get; set; }
        public bool active { get; set; }
    }
}