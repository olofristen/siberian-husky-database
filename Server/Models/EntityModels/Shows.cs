using System;

namespace SHDatabase.Models.EntityModels
{
    public class Shows
    {
        public int ID { get; set; }
        public DateTime date { get; set; }
        public int type { get ; set; }
    }
}