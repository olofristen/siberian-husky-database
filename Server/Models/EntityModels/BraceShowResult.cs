namespace SHDatabase.Models.EntityModels
{
    public class BraceShowResult
    {
        public int showID { get; set; }
        public int M_ID { get; set; }
        public int F_ID { get; set; }
        public int breedPlacement { get; set; }
        public bool HP { get; set; }
        public int BIS { get; set; }
    }
}