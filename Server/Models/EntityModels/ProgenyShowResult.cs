namespace SHDatabase.Models.EntityModels
{
    public class ProgenyShowResult
    {
        public int showID { get; set; }
        public int dogID { get; set; }
        public int breedPlacement { get; set; }
        public bool HP { get; set; }
        public int BIS { get; set; }
    }
}