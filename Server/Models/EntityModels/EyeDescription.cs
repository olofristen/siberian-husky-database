using System;

namespace SHDatabase.Models.EntityModels
{
    public class EyeDescription
    {
        public int ID { get; set; }
        public string Description { get; set; }
    }
}