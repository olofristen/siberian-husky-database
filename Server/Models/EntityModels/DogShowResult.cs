namespace SHDatabase.Models.EntityModels
{
    public class DogShowResult
    {
        public int showID { get; set; }
        public int dogID { get; set; }
        public int showClass { get; set; }
        public int grade { get; set; }
        public int classPlacement { get; set; }
        public bool CK { get; set; }
        public int best_M_Placement { get; set; }
        public int best_F_Placement { get; set; }
        public bool BOS { get; set; }
        public bool BOB { get; set; }
        public bool BOSV { get; set; }
        public bool BOBV { get; set; }
        public bool JCAC { get; set; }
        public bool CAC { get; set; }
        public bool VCAC { get; set; }
        public bool NLCAC { get; set; }
        public bool CACIB { get; set; }
        public bool RCARIB { get; set; }
        public int RW { get; set; }
        public int BIG { get; set; }
        public int BIS { get; set; }
        public int BVIS { get; set; }
        public int BJIS { get; set; }
    }
}