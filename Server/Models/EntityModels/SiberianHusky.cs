using System;

namespace SHDatabase.Models.EntityModels
{
    public class SiberianHusky
    {
        public int ID { get; set; }
        public int status { get; set; }
        public string pedName { get; set; }
        public string callName { get; set; }
        public string pedNum {get; set; }
        public string DOB { get; set; }
        public string microChip { get; set; }
        public int gender { get; set; }
        public string color { get; set; }
        public string orgCountry { get; set; }
        public string currCountry { get; set; }
        public string HD { get; set; }
        public string titles { get; set; }
        public string shortTitles { get; set; }
        public string postTitles { get; set; }
        public bool breedingStatus { get; set; }
        public bool dead { get; set; }
        public string year { get; set; }
    }
}