namespace SHDatabase.Models.EntityModels
{
    public class ShowGrade
    {
        public int grade { get; set; }
        public string description { get; set; }
    }
}