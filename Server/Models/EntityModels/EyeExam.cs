using System;

namespace SHDatabase.Models.EntityModels
{
    public class EyeExam
    {
        public int ID { get; set; }
        public int dogID { get; set; }
        public string date { get; set; }
        public string comment { get; set; }
    }
}