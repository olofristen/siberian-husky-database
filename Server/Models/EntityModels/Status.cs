namespace SHDatabase.Models.EntityModels
{
    public class Status
    {
        public char status { get; set; }
        public string description { get; set; }
    }
}