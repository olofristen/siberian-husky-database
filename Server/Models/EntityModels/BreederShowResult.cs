namespace SHDatabase.Models.EntityModels
{
    public class BreederShowResult
    {
        public int showID { get; set; }
        public int breederID { get; set; }
        public int breedPlacement { get; set; }
        public bool HP { get; set; }
        public int BIS { get; set; }
    }
}