namespace SHDatabase.Models.EntityModels
{
    public class DogBreeder
    {
        public int dogID { get; set; }
        public int breederID { get; set; }
    }
}