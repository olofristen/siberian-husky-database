namespace SHDatabase.Models.EntityModels
{
    public class Image
    {
        public int dogID { get; set; }
        public string imageLink { get; set; }
    }
}