namespace SHDatabase.Models.EntityModels
{
    public class EyeResult
    {
        public int examID { get; set; }
        public int result { get; set; }
        public string other { get; set; }
    }
}