using System.Collections.Generic;

namespace SHDatabase.Models.ViewModels
{
    public class SiberianEyeViewModel
    {
        public int siberianID { get; set; }
        public string comment { get; set; }
        public string other { get; set; }
        public IEnumerable<int> results { get; set; }
    }
}