namespace SHDatabase.Models.ViewModels
{
    public class OwnerAgreementViewModel
    {
        public int id { get; set; }
        public bool agreement { get; set; }
    }
}