namespace SHDatabase.Models.ViewModels
{
    public class BreederViewModel
    {
        public string kennelName { get; set; }
        public string breederName { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string website { get; set; }
    }
}