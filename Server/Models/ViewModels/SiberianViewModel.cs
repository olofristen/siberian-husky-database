using System.Collections.Generic;

namespace SHDatabase.Models.ViewModels
{
    public class SiberianViewModel
    {
        public int status { get; set; }
        public int gender { get; set; }
        public string titles { get; set; }
        public string shortTitles { get; set; }
        public string postTitles { get; set; }
        public string pedigreeName { get; set; }
        public string callName { get; set; }
        public string DOB { get; set; }
        public string microChip { get; set; }
        public string pedigreeNumber { get; set; }
        public IEnumerable<OwnerViewModel> owners { get; set; }
        public IEnumerable<OwnerViewModel> breeders { get; set; }
        public string orgCountry { get; set; }
        public string currCountry { get; set; }
        public string HD { get; set; }
        public string color { get; set; }
        public int sire { get; set; }
        public int dam { get; set; }
        public bool dead { get; set; }
        public string year { get; set; }
    }
}