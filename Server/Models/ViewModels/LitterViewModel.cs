using System.Collections.Generic;

namespace SHDatabase.Models.ViewModels
{
    public class LitterViewModel
    {
        public int ISL_BreederId { get; set; }
        public IEnumerable<OwnerViewModel> breeders { get; set; }
        public string DOB { get; set; }
        public int sire { get; set; }
        public int dam { get; set; }
        public IEnumerable<PuppyViewModel> puppies { get; set; }
    }
}