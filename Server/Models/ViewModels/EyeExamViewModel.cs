using System.Collections.Generic;

namespace SHDatabase.Models.ViewModels
{
    public class EyeExamViewModel
    {
        public string date { get; set; }
        public IEnumerable<SiberianEyeViewModel> siberians { get; set; }
    }
}