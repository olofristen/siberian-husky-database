using System.Collections.Generic;

namespace SHDatabase.Models.ViewModels
{
    public class PuppyViewModel
    {
        public string pedigreeName { get; set; }
        public IEnumerable<OwnerViewModel> owners { get; set; }
        public string microChip { get; set; }
        public int gender { get; set; }
        public string callName { get; set; }
        public string pedigreeNumber { get; set; }
        public string color { get; set; }
    }
}