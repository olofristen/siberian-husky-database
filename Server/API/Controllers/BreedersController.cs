using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SHDatabase.Models.ViewModels;
using SHDatabase.Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class BreedersController : Controller
    {
        private readonly IBreedersServices _breedersService;

        public BreedersController(IBreedersServices breedersService)
        {
            _breedersService = breedersService;
        }

        // GET api/breeders
        [HttpGet("")]
        public IActionResult GetBreeders()
        {
            var breeders = _breedersService.GetBreeders();

            return Ok(breeders);
        }

        // GET api/breeders
        [HttpGet("simple")]
        public IActionResult GetBreedersSimple()
        {
            var breeders = _breedersService.GetBreedersSimple();

            return Ok(breeders);
        }

        // POST api/breeders
        [HttpPost("")]
        public IActionResult PostBreeder([FromBody] BreederViewModel newBreeder)
        {            
            if (newBreeder == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return StatusCode(412);
            }

            var breederId = _breedersService.PostBreeder(newBreeder);
			var breeder = _breedersService.GetBreederById(breederId);
			if (breederId != 0)
			{
				return CreatedAtRoute("GetBreederById", new {breeder_id = breederId}, breeder);
			}
			else
			{
				return StatusCode(412, "something went wrong");
			}
        }

        // GET api/breeders/1
        [HttpGet("{breederId}", Name = "GetBreederById")]
        public IActionResult GetBreederById(int breederId)
        {
            var breeder = _breedersService.GetBreederById(breederId);

            if (breeder == null)
            {
                return NotFound("Breeder is not in the database");
            }

            return Ok(breeder);
        }

        
    }
}
