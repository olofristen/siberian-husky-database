using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SHDatabase.Models.ViewModels;
using SHDatabase.Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class OwnersController : Controller
    {
        private readonly IOwnersServices _ownersService;

        public OwnersController(IOwnersServices ownersService)
        {
            _ownersService = ownersService;
        }

        // GET api/owners
        [HttpGet("")]
        public IActionResult GetOwnerList()
        {
            var owners = _ownersService.GetOwnerList();

            if (owners == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(owners);
            }
        }

        // GET api/owners/agreement
        [HttpGet("agreement")]
        public IActionResult GetOwnerAgreements()
        {
            var owners = _ownersService.GetOwnerAgreements();

            return Ok(owners);
        }

        // PUT api/owners/agreement
        [HttpPut("agreement")]
        public IActionResult PostOwnerAgreement([FromBody] List<OwnerAgreementViewModel> newAgreement)
        {
            if (newAgreement == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return StatusCode(412);
            }

            var response = _ownersService.PostOwnerAgreements(newAgreement);

            if (response)
            {
                return CreatedAtAction("Saved", response);
            }
            else
            {
                return StatusCode(412);
            }
        }
    }
}