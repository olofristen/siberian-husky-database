using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SHDatabase.Models.ViewModels;
using SHDatabase.Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class OtherController : Controller
    {
        private readonly IOtherServices _otherService;

        public OtherController(IOtherServices otherService)
        {
            _otherService = otherService;
        }

        // GET api/other/country
        [HttpGet("country")]
        public IActionResult GetCountries()
        {
            var countries = _otherService.GetCountries();

            return Ok(countries);
        }
    }
}