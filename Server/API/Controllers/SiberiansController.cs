﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SHDatabase.Models.ViewModels;
using SHDatabase.Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class SiberiansController : Controller
    {
        private readonly ISiberiansServices _siberiansService;

        public SiberiansController(ISiberiansServices siberiansService)
        {
            _siberiansService = siberiansService;
        }

        // GET api/siberians
        [HttpGet("", Name="GetSiberians")]
        public IActionResult GetSiberians()
        {
            var siberians = _siberiansService.GetSiberians();
            
            if (siberians == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(siberians);
            }
        }

        // GET api/siberians/all
        [HttpGet("all")]
        public IActionResult GetAllSiberians()
        {
            var siberians = _siberiansService.GetAllSiberians();
            
            if (siberians == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(siberians);
            }
        }

        // GET api/siberians/is
        [HttpGet("is")]
        public IActionResult GetIcelandicSiberians()
        {
            var siberians = _siberiansService.GetIcelandicSiberians();

            if (siberians == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(siberians);
            }
        }

        // GET api/siberians/males
        [HttpGet("males")]
        public IActionResult GetMales()
        {
            var males = _siberiansService.GetMales();

            if (males == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(males);
            }
        }

        // GET api/siberians/females
        [HttpGet("females")]
        public IActionResult GetFemales()
        {
            var females = _siberiansService.GetFemales();

            if (females == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(females);
            }
        }

        // GET api/siberians/1
        [HttpGet("{id:int}", Name = "GetSiberianById")]
        public IActionResult GetSiberianById(int id)
        {
            var siberian = _siberiansService.GetSiberianById(id);
            
            if (siberian == null)
            {
                return NotFound();
            }
            else
            {
                return new ObjectResult(siberian);
            }
        }

        // GET api/siberians/1/parents
        [HttpGet("{id:int}/parents")]
        public IActionResult GetParentsOfSiberianByID(int id)
        {
            var parents = _siberiansService.GetParentsOfSiberianByID(id);
            
            if (parents == null)
            {
                return NotFound();
            }
            else
            {
                return new ObjectResult(parents);
            }            
        }

        // GET api/siberians/1/progeny
        [HttpGet("{id:int}/progeny")]
        public IActionResult GetProgenyList(int id)
        {
            var progeny = _siberiansService.GetProgenyList(id);

            if (progeny == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(progeny);
            }
        }

        // GET api/siberians/1/litters
        [HttpGet("{id:int}/litters")]
        public IActionResult GetLitters(int id)
        {
            var litters = _siberiansService.GetLitters(id);

            if (litters == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(litters);
            }
        }

        // POST api/siberians
        [HttpPost("")]
        public IActionResult PostSiberian([FromBody] SiberianViewModel newSiberian)
        {
            if (newSiberian == null)
			{
                Console.WriteLine("BAD REQUEST");
				return BadRequest();
			}

			if (!ModelState.IsValid)
			{
                Console.WriteLine("MODELSTATE INVALID");
				return StatusCode(412);
			}

            Console.WriteLine("POSTING...");
			var newSiberianId = _siberiansService.PostSiberian(newSiberian);
            if (newSiberianId == 0)
            {
                return StatusCode(412);
            }

			var siberian = _siberiansService.GetSiberianById(newSiberianId);
			if (newSiberianId != 0)
			{
				return CreatedAtRoute("GetSiberianById", new {id = newSiberianId}, siberian);
			}
			else
			{
				return StatusCode(412, "something went wrong");
			}
        }

        // POST api/siberians/litter
        [HttpPost("litter")]
        public IActionResult PostLitter([FromBody] LitterViewModel newLitter)
        {
            if (newLitter == null)
			{
				return BadRequest();
			}

			if (!ModelState.IsValid)
			{
				return StatusCode(412);
			}

            Console.WriteLine("POSTING LITTER...");
			var response = _siberiansService.PostLitter(newLitter);
			if (response != 0)
			{
				return CreatedAtRoute("GetSiberians", response);
			}
			else
			{
				return StatusCode(412, "something went wrong");
			}
        }

    }
}
