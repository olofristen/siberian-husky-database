using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SHDatabase.Models.ViewModels;
using SHDatabase.Services;

namespace API.Controllers
{
    [Route("api/[controller]")]
    public class EyeExamController : Controller
    {
        private readonly IOtherServices _otherService;

        public EyeExamController(IOtherServices otherService)
        {
            _otherService = otherService;
        }

        // POST api/eyeexam
        [HttpPost("")]
        public IActionResult PostLitter([FromBody] EyeExamViewModel newExam)
        {
            if (newExam == null)
			{
				return BadRequest();
			}

			if (!ModelState.IsValid)
			{
				return StatusCode(412);
			}

			var response = _otherService.PostEyeExam(newExam);
			if (response != 0)
			{
				return Ok(response);
			}
			else
			{
				return StatusCode(412, "something went wrong");
			}
        }
    }
}