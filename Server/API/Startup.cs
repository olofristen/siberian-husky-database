﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SHDatabase.Models;
using SHDatabase.Repositories;
using SHDatabase.Services;

namespace SHDatabase
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add application services
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<ISiberiansServices, SiberiansServices>();
            services.AddTransient<IBreedersServices, BreedersServices>();
            services.AddTransient<IOtherServices, OtherServices>();
            services.AddTransient<IOwnersServices, OwnersServices>();

            // Add application repositories
            services.AddTransient<ISiberiansRepository, SiberiansRepository>();
            services.AddTransient<IOwnersRepository, OwnersRepository>();

            services.AddMvc();

            services.AddDbContext<ApplicationDbContext>(options => 
                options.UseSqlite("Data Source=../Repositories/SHDatabase.db", b => b.MigrationsAssembly("API")));
                // FOR MYSQL USE THIS TYPE OF STRING:
                // optionsBuilder.UseMySQL("server=aspiresiberianscom2.ipagemysql.com;database=shdatabase;user=huskydeild;password=shdeild14789");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
           app.Use(async (context, next) => {
            await next();
            if (context.Response.StatusCode == 404 &&
                !Path.HasExtension(context.Request.Path.Value) &&
                !context.Request.Path.Value.StartsWith("/api/")) {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });
            app.UseMvcWithDefaultRoute();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseAuthentication();
        }
    }
}
