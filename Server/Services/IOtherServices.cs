using System;
using System.Collections.Generic;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.ViewModels;

namespace SHDatabase.Services
{
    public interface IOtherServices
    {
        IEnumerable<CountryDTO> GetCountries();
        int PostEyeExam(EyeExamViewModel newExam);
    }
}
