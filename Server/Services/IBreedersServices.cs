using System;
using System.Collections.Generic;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.ViewModels;

namespace SHDatabase.Services
{
    public interface IBreedersServices
    {
        int PostBreeder(BreederViewModel newBreeder);
        BreederDetailDTO GetBreederById(int breederId);
        BreederListDTO GetBreeders();
        IEnumerable<BreederSimpleDTO> GetBreedersSimple();
    }
}
