﻿using System;
using System.Collections.Generic;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.ViewModels;

namespace SHDatabase.Services
{
    public interface ISiberiansServices
    {
        IEnumerable<SiberianListDTO> GetSiberians();
        IEnumerable<SiberianDTO> GetAllSiberians();
        IEnumerable<SiberianSimpleDTO> GetIcelandicSiberians();
        IEnumerable<SiberianSimpleDTO> GetMales();
        IEnumerable<SiberianSimpleDTO> GetFemales();
        IEnumerable<SiberianListDTO> ListSiberianByYear(IEnumerable<SiberianDTO> siberians);
        SiberianDetailsDTO GetSiberianById(int id);
        ParentsIDDTO GetParentsOfSiberianByID(int dogID);
        IEnumerable<ParentsIDDTO> GetProgenyList(int id);
        IEnumerable<LitterSimpleDTO> GetLitters(int id);
        int PostSiberian(SiberianViewModel newSiberian);
        int PostLitter(LitterViewModel newLitter);
    }
}
