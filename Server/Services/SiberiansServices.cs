using System;
using System.Collections.Generic;
using System.Linq;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;
using SHDatabase.Models.ViewModels;
using SHDatabase.Repositories;

namespace SHDatabase.Services
{
    public class SiberiansServices : ISiberiansServices
    {
        private readonly ISiberiansRepository _repo;
        private readonly IOwnersRepository _ownerRepo;

        public SiberiansServices(ISiberiansRepository repo, IOwnersRepository ownerRepo)
        {
            _repo = repo;
            _ownerRepo = ownerRepo;
        }

        public IEnumerable<SiberianListDTO> GetSiberians()
        {
            var siberians = _repo.GetSiberians();
            DateTime date = DateTime.Now.AddYears(-1);

            // Check if the eye examination is valid (less than year old)
            foreach (var sibe in siberians)
            {
               if (sibe.Eye != null)
               {
                   DateTime eye = Convert.ToDateTime(sibe.Eye);
                   if (eye > date)
                   {
                       sibe.EyeValid = true;
                   }
               }
            }

            var siberianList = ListSiberianByYear(siberians);
            return siberianList;
        }

        public IEnumerable<SiberianSimpleDTO> GetIcelandicSiberians()
        {
            var siberians = _repo.GetIcelandicSiberians();
            return siberians;
        }

        public IEnumerable<SiberianListDTO> ListSiberianByYear(IEnumerable<SiberianDTO> siberians)
        {
            List<SiberianListDTO> sibeList = new List<SiberianListDTO>();

            foreach (var siberian in siberians)
            {
                SiberianListDTO tempYear = new SiberianListDTO();
                List<SiberianDTO> tempList = new List<SiberianDTO>();
                tempYear.siberianList = tempList;

                tempYear.year = siberian.Year;

                bool exists = sibeList.Any(item => item.year == tempYear.year);

                if (!exists)
                {
                    sibeList.Add(tempYear);
                }

                foreach (var year in sibeList)
                {
                    if (year.year == siberian.Year)
                    {
                        SiberianDTO tempSiberian = new SiberianDTO {
                            ID = siberian.ID,
                            Status = siberian.Status,
                            Titles = siberian.Titles,
                            PedigreeName = siberian.PedigreeName,
                            DOB = siberian.DOB,
                            HD = siberian.HD,
                            Eye = siberian.Eye,
                            Dead = siberian.Dead,
                            Year = siberian.Year,
                            BreedingStatus = siberian.BreedingStatus,
                            EyeValid = siberian.EyeValid
                        };
                        
                        year.siberianList.Add(tempSiberian);
                    }
                }
            }

            return sibeList;
        }

        public IEnumerable<SiberianSimpleDTO> GetMales()
        {
            var males = _repo.GetMales();
            return males;
        }

        public IEnumerable<SiberianSimpleDTO> GetFemales()
        {
            var females = _repo.GetFemales();
            return females;
        }

        public SiberianDetailsDTO GetSiberianById(int id)
        {
            var siberian = _repo.GetSiberianById(id);
            return siberian;
        }

        public ParentsIDDTO GetParentsOfSiberianByID(int id)
        {
            var parents = _repo.GetParentsOfSiberianByID(id);
            return parents;
        }

        public IEnumerable<ParentsIDDTO> GetProgenyList(int id)
        {
            var progeny = _repo.getProgenyList(id);
            return progeny;
        }

        public IEnumerable<LitterSimpleDTO> GetLitters(int id)
        {
            var litters = _repo.getProgeny(id);
            return litters;
        }

        public int PostSiberian(SiberianViewModel newSiberian)
        {
            if (newSiberian.status < 1 
                || newSiberian.pedigreeName == null
                || newSiberian.gender < 1)
            {
                return 0;
            }

            if (newSiberian.DOB == null)
            {
                newSiberian.DOB = "0001-01-01";
            }

            if (newSiberian.status == 3 && newSiberian.orgCountry == null)
            {
                newSiberian.orgCountry = "IS";
            }

            if (newSiberian.year == null && newSiberian.status == 3 && newSiberian.DOB != null)
            {
                DateTime date = Convert.ToDateTime(newSiberian.DOB);
                var year = date.Year;
                newSiberian.year = year.ToString();
            }

            SiberianHusky siberian = new SiberianHusky
            {
                status = newSiberian.status,
                pedName = newSiberian.pedigreeName,
                callName = newSiberian.callName,
                pedNum = newSiberian.pedigreeNumber,
                DOB = newSiberian.DOB,
                microChip = newSiberian.microChip,
                gender = newSiberian.gender,
                color = newSiberian.color,
                orgCountry = newSiberian.orgCountry,
                currCountry = newSiberian.currCountry,
                HD = newSiberian.HD,
                titles = newSiberian.titles,
                shortTitles = newSiberian.shortTitles,
                postTitles = newSiberian.postTitles,
                dead = newSiberian.dead,
                year = newSiberian.year
            };

            var siberianId = _repo.PostSiberian(siberian);

            if (siberianId == 0)
            {
                return siberianId;
            }

            if (newSiberian.sire > 1 || newSiberian.dam > 1)
            {
                Parents parents = new Parents
                {
                    dogID = siberianId,
                    sireID = newSiberian.sire,
                    damID = newSiberian.dam
                };

                _repo.PostParents(parents);
            }

            IEnumerable<OwnerListDTO> ownerList = this._ownerRepo.GetOwnerList().ToArray();
            List<OwnerViewModel> puppyOwners = new List<OwnerViewModel>();
            puppyOwners = newSiberian.owners.ToList();
            for (int i = 0; i < puppyOwners.Count(); i++)
            {
                var tempOwner = ownerList.FirstOrDefault(o => o.name == puppyOwners[i].name);
                var ownerID = 0;
                if (tempOwner == null)
                {
                    Owner info = new Owner()
                    {
                        name = puppyOwners[i].name,
                        country = puppyOwners[i].country,
                        agreement = false
                    };

                    ownerID = _ownerRepo.PostOwner(info);

                    OwnerListDTO newOwner = new OwnerListDTO()
                    {
                        ID = ownerID,
                        name = info.name,
                        country = info.country
                    };

                    ownerList = ownerList.Concat(new[] { newOwner });
                }

                if (ownerID == 0)
                {
                    ownerID = tempOwner.ID;
                }

                DogOwner connection = new DogOwner
                {
                    dogID = siberianId,
                    ownerID = ownerID
                };

                _ownerRepo.PostOwnerDogConnection(connection);
            }

            List<OwnerViewModel> puppyBreeders = new List<OwnerViewModel>();
            puppyBreeders = newSiberian.breeders.ToList();
            for (int i = 0; i < puppyBreeders.Count(); i++)
            {
                var tempBreeder = ownerList.FirstOrDefault(o => o.name == puppyBreeders[i].name);
                var breederID = 0;
                if (tempBreeder == null)
                {
                    Owner info = new Owner()
                    {
                        name = puppyBreeders[i].name,
                        country = puppyBreeders[i].country,
                        agreement = false
                    };

                    breederID = _ownerRepo.PostOwner(info);

                    OwnerListDTO newBreeder = new OwnerListDTO()
                    {
                        ID = breederID,
                        name = info.name,
                        country = info.country
                    };

                    ownerList = ownerList.Concat(new[] { newBreeder });
                }

                if (breederID == 0)
                {
                    breederID = tempBreeder.ID;
                }

                DogBreeder connection = new DogBreeder
                {
                    dogID = siberianId,
                    breederID = breederID
                };

                _ownerRepo.PostBreederDogConnection(connection);
            }


            return siberianId;
        }

        public int PostLitter(LitterViewModel newLitter)
        {
            if (newLitter.puppies.Count() == 0)
            {
                return 0;
            }
            else
            {
                DateTime date = Convert.ToDateTime(newLitter.DOB);
                var year = date.Year;
                IEnumerable<OwnerListDTO> ownerList = this._ownerRepo.GetOwnerList().ToArray();

                foreach(var puppy in newLitter.puppies)
                {
                    SiberianHusky siberian = new SiberianHusky
                    {
                        status = 3,
                        pedName = puppy.pedigreeName,
                        callName = puppy.callName,
                        pedNum = puppy.pedigreeNumber,
                        DOB = newLitter.DOB,
                        microChip = puppy.microChip,
                        gender = puppy.gender,
                        color = puppy.color,
                        orgCountry = "IS",
                        currCountry = "IS",
                        dead = false,
                        year = year.ToString()
                    };

                    var siberianId = _repo.PostSiberian(siberian);

                    if (siberianId == 0)
                    {
                        return siberianId;
                    }

                    if (newLitter.sire > 1 || newLitter.dam > 1)
                    {
                        Parents parents = new Parents
                        {
                            dogID = siberianId,
                            sireID = newLitter.sire,
                            damID = newLitter.dam
                        };

                        _repo.PostParents(parents);
                    }

                    List<OwnerViewModel> puppyOwners = new List<OwnerViewModel>();
                    puppyOwners = puppy.owners.ToList();
                    for (int i = 0; i < puppyOwners.Count(); i++)
                    {
                        var tempOwner = ownerList.FirstOrDefault(o => o.name == puppyOwners[i].name);
                        var ownerID = 0;
                        if (tempOwner == null)
                        {
                            Owner info = new Owner()
                            {
                                name = puppyOwners[i].name,
                                country = puppyOwners[i].country,
                                agreement = false
                            };

                            ownerID = _ownerRepo.PostOwner(info);

                            OwnerListDTO newOwner = new OwnerListDTO()
                            {
                                ID = ownerID,
                                name = info.name,
                                country = info.country
                            };

                            ownerList = ownerList.Concat(new[] { newOwner });
                        }

                        if (ownerID == 0)
                        {
                            ownerID = tempOwner.ID;
                        }

                        DogOwner connection = new DogOwner
                        {
                            dogID = siberianId,
                            ownerID = ownerID
                        };

                        _ownerRepo.PostOwnerDogConnection(connection);
                    }

                    List<OwnerViewModel> puppyBreeders = new List<OwnerViewModel>();
                    puppyBreeders = newLitter.breeders.ToList();
                    for (int i = 0; i < puppyBreeders.Count(); i++)
                    {
                        var tempBreeder = ownerList.FirstOrDefault(o => o.name == puppyBreeders[i].name);
                        var breederID = 0;
                        if (tempBreeder == null)
                        {
                            Owner info = new Owner()
                            {
                                name = puppyBreeders[i].name,
                                country = puppyBreeders[i].country,
                                agreement = false
                            };

                            breederID = _ownerRepo.PostOwner(info);

                            OwnerListDTO newBreeder = new OwnerListDTO()
                            {
                                ID = breederID,
                                name = info.name,
                                country = info.country
                            };

                            ownerList = ownerList.Concat(new[] { newBreeder });
                        }

                        if (breederID == 0)
                        {
                            breederID = tempBreeder.ID;
                        }

                        DogBreeder connection = new DogBreeder
                        {
                            dogID = siberianId,
                            breederID = breederID
                        };

                        _ownerRepo.PostBreederDogConnection(connection);
                    }

                }
            }
            return 1;
        }

        public IEnumerable<SiberianDTO> GetAllSiberians()
        {
            return _repo.GetAllSiberians();
        }
    }
}