using System;
using System.Collections.Generic;
using System.Linq;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;
using SHDatabase.Models.ViewModels;
using SHDatabase.Repositories;

namespace SHDatabase.Services
{
    public class OtherServices : IOtherServices
    {
        private readonly ISiberiansRepository _repo;

        public OtherServices(ISiberiansRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<CountryDTO> GetCountries()
        {
            var countries = _repo.GetCountries();
            return countries;
        }

        public int PostEyeExam(EyeExamViewModel newExam)
        {
            if (newExam.siberians.Count() == 0)
            {
                return 0;
            }
            else
            {
                foreach (var siberian in newExam.siberians)
                {
                    EyeExam exam = new EyeExam
                    {
                        dogID = siberian.siberianID,
                        date = newExam.date,
                        comment = siberian.comment
                    };

                    var newExamID = _repo.PostEyeExam(exam);

                    if (siberian.results.Count() != 0)
                    {
                        foreach (var res in siberian.results)
                        {
                            EyeResult eyeResult = new EyeResult
                            {
                                examID = newExamID,
                                result = res,
                                other = siberian.other
                            };

                            _repo.PostEyeResult(eyeResult);
                        }
                    }
                }

                return 1;
            }
        }
    }
}
