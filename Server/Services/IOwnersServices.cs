using System;
using System.Collections.Generic;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.ViewModels;

namespace SHDatabase.Services
{
    public interface IOwnersServices
    {
        IEnumerable<OwnerListDTO> GetOwnerList();
        IEnumerable<OwnerAgreementDTO> GetOwnerAgreements();
        bool PostOwnerAgreements(List<OwnerAgreementViewModel> newAgreements);
    }
}
