using System;
using System.Collections.Generic;
using System.Linq;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;
using SHDatabase.Models.ViewModels;
using SHDatabase.Repositories;

namespace SHDatabase.Services
{
    public class BreedersServices : IBreedersServices
    {
        private readonly ISiberiansRepository _repo;

        public BreedersServices(ISiberiansRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<BreederSimpleDTO> GetBreedersSimple()
        {
            var breeders = _repo.GetBreeders();
            return breeders;
        }

        public BreederListDTO GetBreeders()
        {
            var breeders = _repo.GetBreeders();
            DateTime dateNow = DateTime.Now;

            var active = new List<BreederSimpleDTO>();
            var inactive = new List<BreederSimpleDTO>();

            foreach (var breeder in breeders)
            {
                if (breeder.active)
                {
                    active.Add(breeder);
                }
                else 
                {
                    inactive.Add(breeder);
                }
            }

            BreederListDTO activeBreeders = new BreederListDTO()
            {
                active = active,
                inactive = inactive
            };

            return activeBreeders;
        }

        public int PostBreeder(BreederViewModel newBreeder)
        {
            Kennel breeder = new Kennel
            {
                kennelName = newBreeder.kennelName,
                email = newBreeder.email,
                phone = newBreeder.phone,
                website = newBreeder.website
            };

            var breederID = _repo.PostBreeder(breeder);

            return breederID;
        }

        public BreederDetailDTO GetBreederById(int breederId)
        {
            var breeder = _repo.GetBreederById(breederId);

            return breeder;
        }

    }
}
