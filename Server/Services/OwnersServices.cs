using System;
using System.Collections.Generic;
using System.Linq;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;
using SHDatabase.Models.ViewModels;
using SHDatabase.Repositories;

namespace SHDatabase.Services
{
    public class OwnersServices : IOwnersServices
    {
        private readonly IOwnersRepository _repo;

        public OwnersServices(IOwnersRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<OwnerListDTO> GetOwnerList()
        {
            var owners = _repo.GetOwnerList();
            return owners;
        }

        public IEnumerable<OwnerAgreementDTO> GetOwnerAgreements()
        {
            var owners = _repo.GetOwnerAgreements();
            return owners;
        }

        public bool PostOwnerAgreements(List<OwnerAgreementViewModel> newAgreements)
        {
            var res = true;
            foreach (var owner in newAgreements)
            {
                var response = _repo.PostOwnerAgreements(owner);
                if (!response)
                {
                    res = false;
                }
            }
            return res;
        }
    }
}