using System;
using System.Collections.Generic;
using System.Linq;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;
using SHDatabase.Models.ViewModels;

namespace SHDatabase.Repositories
{
    public class OwnersRepository : IOwnersRepository
    {
        private readonly ApplicationDbContext _db;

        public OwnersRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<OwnerListDTO> GetOwnerList()
        {
            var owners = (from o in _db.Owners
                            select new OwnerListDTO
                            {
                                ID = o.ID,
                                name = o.name,
                                country = o.country
                            }).ToList();

            return owners;
        }

        public IEnumerable<OwnerAgreementDTO> GetOwnerAgreements()
        {
            var owners = (from o in _db.Owners
                            where o.country == "IS"
                            select new OwnerAgreementDTO
                            {
                                ID = o.ID,
                                name = o.name,
                                agreement = o.agreement
                            }).ToList();
            
            return owners;
        }

        public bool PostOwnerAgreements(OwnerAgreementViewModel newAgreement)
        {
            var owner = _db.Owners.SingleOrDefault(o => o.ID == newAgreement.id);

            if (owner == null)
            {
                return false;
            }
            else
            {
                owner.agreement = newAgreement.agreement;
                _db.SaveChanges();
                return true;
            }
        }

        public void PostOwnerDogConnection(DogOwner newConnection)
        {
            if (newConnection == null)
            {
               Console.WriteLine("No new dog-owner connection...");
            }

            _db.DogOwner.Add(newConnection);
            _db.SaveChanges();
        }

        public int PostOwner(Owner newOwner)
        {
            if (newOwner == null)
            {
                return 0;
            }

            _db.Owners.Add(newOwner);
            _db.SaveChanges();

            return newOwner.ID;
        }

        public void PostBreederDogConnection(DogBreeder newConnection)
        {
            if (newConnection == null)
            {
               Console.WriteLine("No new dog-owner connection...");
            }

            _db.DogBreeder.Add(newConnection);
            _db.SaveChanges();
        }
    }
}