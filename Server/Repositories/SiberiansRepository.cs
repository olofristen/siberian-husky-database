﻿using System;
using System.Collections.Generic;
using System.Linq;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;

namespace SHDatabase.Repositories
{
    public class SiberiansRepository : ISiberiansRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly int hidden;

        public SiberiansRepository(ApplicationDbContext db)
        {
            _db = db;

            hidden = 4;

            // Create function that will mark dogs as dead if they are 20+
        }

        public IEnumerable<SiberianDTO> GetSiberians()
        {
            var siberians = (from s in _db.Siberians
                                join g in _db.Genders
                                on s.gender equals g.shortGender into gen
                                from g in gen
                                where s.status != hidden
                                orderby s.ID
                                select new SiberianDTO
                                {
                                    ID = s.ID,
                                    Status = s.status,
                                    Titles = s.shortTitles,
                                    PedigreeName = s.pedName,
                                    DOB = s.DOB,
                                    HD = s.HD,
                                    Eye = (from e in _db.EyeExams
                                            where s.ID == e.dogID
                                            orderby e.date descending
                                            select e.date).FirstOrDefault(),
                                    Dead = s.dead,
                                    Year = s.year,
                                    BreedingStatus = s.breedingStatus,
                                    EyeValid = false
                                }).ToList();
            
            return siberians;
        }

        public IEnumerable<SiberianSimpleDTO> GetIcelandicSiberians()
        {
            var siberians = (from s in _db.Siberians
                                where s.status != hidden
                                select new SiberianSimpleDTO
                                {
                                    ID = s.ID,
                                    PedigreeName = s.pedName,
                                    Titles = s.titles
                                }).ToList();
            
            return siberians;
        }

        public IEnumerable<SiberianSimpleDTO> GetMales()
        {
            var males = (from s in _db.Siberians
                            where s.gender == 2
                            select new SiberianSimpleDTO
                            {
                                ID = s.ID,
                                Titles = s.titles,
                                PedigreeName = s.pedName
                            });

            return males;
        }

        public IEnumerable<SiberianSimpleDTO> GetFemales()
        {
            var females = (from s in _db.Siberians
                where s.gender == 1
                select new SiberianSimpleDTO 
                {
                    ID = s.ID,
                    Titles = s.titles,
                    PedigreeName = s.pedName
                });
                            
            return females;
        }

        public string GetGenderById(int id)
        {
            var siberian = GetSiberianById(id);

            if (siberian == null)
            {
                return null;
            }

            return siberian.Gender;
        }

        public SiberianDetailsDTO GetSiberianById(int id)
        {
            List<SiberianSimpleDTO> ProgenyList = new List<SiberianSimpleDTO>();
            
            var siberian = (from s in _db.Siberians
                                join g in _db.Genders
                                on s.gender equals g.shortGender into gen
                                from g in gen
                                where s.ID == id
                                select new SiberianDetailsDTO
                                {
                                    ID = s.ID,
                                    Status = s.status,
                                    Titles = s.titles,
                                    ShortTitles = s.shortTitles,
                                    PostTitles = s.postTitles,
                                    PedigreeName = s.pedName,
                                    CallName = s.callName,
                                    DOB = s.DOB,
                                    MicroChip = s.microChip,
                                    Gender = g.description,
                                    PedigreeNumber = s.pedNum,
                                    Owners = (from d in _db.DogOwner
                                                join o in _db.Owners
                                                on d.ownerID equals o.ID
                                                where (o.agreement == true
                                                || o.country != "IS")
                                                && s.ID == d.dogID
                                                select o.name
                                            ).ToList(),
                                    Breeders = (from b in _db.DogBreeder
                                                join o in _db.Owners
                                                on b.breederID equals o.ID
                                                where s.ID == b.dogID
                                                select o.name).ToList(),
                                    Year = s.year,
                                    OrgCountry = (from c in _db.Countries
                                                    where s.orgCountry == c.id
                                                    select c.value).SingleOrDefault(),
                                    CurrCountry = (from c in _db.Countries
                                                    where s.currCountry == c.id
                                                    select c.value).SingleOrDefault(),
                                    Color = s.color,
                                    HD = s.HD,
                                    BreedingStatus = s.breedingStatus,
                                    Sire = null,
                                    Dam = null,
                                    Eye = (from e in _db.EyeExams
                                            where s.ID == e.dogID
                                            orderby e.date descending
                                            select new EyeResultDTO
                                            {
                                                Date = e.date,
                                                result = (from r in _db.EyeResults
                                                            join d in _db.EyeDescription
                                                            on r.result equals d.ID
                                                            where e.ID == r.examID
                                                            select d.Description).ToList(),
                                                comment = e.comment,
                                                other = (from r in _db.EyeResults
                                                            where e.ID == r.examID
                                                            && (r.result == 7 || r.result == 20)
                                                            select r.other).SingleOrDefault()
                                            }).ToList(),
                                    Shows = null,
                                    Dead = s.dead,
                                    Litters = null
                                }).SingleOrDefault();
            
            var parents = GetParentsOfSiberianByID(id);

            if (parents != null)
            {
                var sire = GetPedigree(parents.SireID);
                var dam = GetPedigree(parents.DamID);

                siberian.Sire = sire;
                siberian.Dam = dam;
            }

            var litters = getProgeny(id);

            if (litters != null)
            {
                siberian.Litters = litters;
            }

            return siberian;
        }

        public ParentsIDDTO GetParentsOfSiberianByID(int dogID)
        {
            if (dogID == 0)
            {
                return null;
            }

            var siberian = (from p in _db.Parents
                                where p.dogID == dogID
                                select new ParentsIDDTO
                                {
                                    DogID = p.dogID,
                                    SireID = p.sireID,
                                    DamID = p.damID
                                }).SingleOrDefault();

            return siberian;
        }

        public IEnumerable<ParentsIDDTO> getProgenyList(int id)
        {
            if (id == 0)
            {
                return null;
            }

            var progeny = (from p in _db.Parents
                            where p.damID == id
                            || p.sireID == id
                            select new ParentsIDDTO
                            {
                                DogID = p.dogID,
                                DamID = p.damID,
                                SireID = p.sireID
                            }).ToList();
            
            return progeny;
        }

        public IEnumerable<LitterSimpleDTO> getProgeny(int dogId)
        {
            if (dogId == 0)
            {
                return null;
            }

            var progeny = getProgenyList(dogId);

            List<LitterSimpleDTO> tempLitters = new List<LitterSimpleDTO>();

            if (progeny != null)
            {
                foreach (var prog in progeny)
                {
                    LitterSimpleDTO tempLitter = new LitterSimpleDTO();
                    var info = (from i in _db.Siberians
                                    where i.ID == prog.DogID
                                    join p in _db.Parents
                                    on i.ID equals p.dogID
                                    select new SiberianBirthInfoDTO 
                                    {
                                        ID = dogId,
                                        PedName = i.pedName,
                                        Titles = i.titles,
                                        DOB = i.DOB,
                                        SireID = p.sireID,
                                        SirePedName = (from s in _db.Siberians
                                                        where s.ID == p.sireID
                                                        select s.pedName).SingleOrDefault(),
                                        DamID = p.damID,
                                        DamPedName = (from s in _db.Siberians
                                                        where s.ID == p.damID
                                                        select s.pedName).SingleOrDefault(),
                                        SireTitles = (from s in _db.Siberians
                                                        where s.ID == p.sireID
                                                        select s.titles).SingleOrDefault(),
                                        DamTitles = (from s in _db.Siberians
                                                        where s.ID == p.damID
                                                        select s.titles).SingleOrDefault()
                                    }).SingleOrDefault();

                    tempLitter.DamID = info.DamID;
                    tempLitter.SireID = info.SireID;
                    tempLitter.DOB = info.DOB;
                    tempLitter.DamPedName = info.DamPedName;
                    tempLitter.SirePedName = info.SirePedName;
                    tempLitter.DamTitles = info.DamTitles;
                    tempLitter.SireTitles = info.SireTitles;
                    tempLitter.Progeny = new List<SiberianSimpleDTO>();

                    bool exists = tempLitters.Any(item => item.DamID == tempLitter.DamID && 
                                                                item.SireID == tempLitter.SireID &&
                                                                item.DOB == tempLitter.DOB);

                    if (!exists)
                    {
                        tempLitters.Add(tempLitter);
                    }

                    foreach (var litter in tempLitters)
                    {
                        if (litter.DamID == info.DamID &&
                            litter.SireID == info.SireID &&
                            litter.DOB == info.DOB)
                        {
                            SiberianSimpleDTO puppy = new SiberianSimpleDTO();
                            puppy.ID = prog.DogID;
                            puppy.PedigreeName = info.PedName;
                            puppy.Titles = info.Titles;

                            litter.Progeny.Add(puppy);
                        }
                    }
                }
            }

            return tempLitters;
        }

        public SiberianForPedigreeDTO GetPedigree(int id)
        {
            if (id == 0)
            {
                return null;
            }

            var parents = GetParentsOfSiberianByID(id);

            var dog = (from s in _db.Siberians
                                where s.ID == id
                                select new SiberianForPedigreeDTO
                                {
                                    DogID = id,
                                    DogTitles = s.titles,
                                    DogPedigreeName = s.pedName,
                                    DogHD = s.HD,
                                    DogEye = null,
                                    DogColor = s.color
                                }).SingleOrDefault();
            
            if (parents != null)
            {
                dog.Sire = GetPedigree(parents.SireID);
                dog.Dam = GetPedigree(parents.DamID);
            }

            return dog;
        }

        public int PostBreeder(Kennel newBreeder)
        {
            _db.Kennels.Add(newBreeder);
            _db.SaveChanges();

            Console.WriteLine("BREEDER ID: " + newBreeder.ID);

            return newBreeder.ID;
        }

        public int PostSiberian(SiberianHusky newSiberian)
        {
            if (newSiberian == null)
            {
                return 0;
            }

            _db.Siberians.Add(newSiberian);
            _db.SaveChanges();

            Console.WriteLine("SIBERIAN ID: " + newSiberian.ID);

            return newSiberian.ID;
        }

        public int PostEyeExam(EyeExam newExam)
        {
            if (newExam == null)
            {
                return 0;
            }

            _db.EyeExams.Add(newExam);
            _db.SaveChanges();

            Console.WriteLine("EYE RESULT ID: " + newExam.ID);

            return newExam.ID;
        }
        
        public void PostEyeResult(EyeResult newEyeResult)
        {
            if (newEyeResult == null)
            {
                Console.WriteLine("No eye result to add...");
            }

            _db.EyeResults.Add(newEyeResult);
            _db.SaveChanges();
        }

        public void PostParents(Parents parents)
        {
            _db.Parents.Add(parents);
            _db.SaveChanges();

            Console.WriteLine("PARENTS SET TO DOG# " + parents.dogID);
        }

        public BreederDetailDTO GetBreederById(int breederId)
        {
            var breeder = (from b in _db.Kennels
                            where b.ID == breederId
                            select new BreederDetailDTO
                            {
                                ID = b.ID,
                                kennelName = b.kennelName,
                                breederName = (from o in _db.Owners
                                                join bo in _db.BreederName
                                                on o.ID equals bo.ownerID
                                                where b.ID == bo.kennelID
                                                select o.name).ToList(),
                                email = b.email,
                                phone = b.phone,
                                website = b.website,
                                puppyCount = 0,
                                owned = (from bo in _db.BreederName
                                        join o in _db.DogOwner
                                        on bo.ownerID equals o.ownerID
                                        join s in _db.Siberians
                                        on o.dogID equals s.ID
                                        where bo.kennelID == b.ID
                                        select new SiberianSimpleDTO
                                        {
                                            ID = s.ID,
                                            Titles = s.shortTitles,
                                            PedigreeName = s.pedName
                                        }).ToList(),
                                bred = null
                            }).SingleOrDefault();

            var puppies = (from n in _db.BreederName
                            join b in _db.DogBreeder
                            on n.ownerID equals b.breederID
                            where n.kennelID == breederId
                            select b.dogID).Distinct().ToList();

            breeder.puppyCount = puppies.Count();

            breeder.bred = GetBreedersLitters(puppies);

            return breeder;
        }

        public IEnumerable<LitterSimpleDTO> GetBreedersLitters(List<int> puppies)
        {
            List<LitterSimpleDTO> tempLitters = new List<LitterSimpleDTO>();

            if (puppies != null)
            {
                foreach (var prog in puppies)
                {
                    LitterSimpleDTO tempLitter = new LitterSimpleDTO();
                    var info = (from i in _db.Siberians
                                    where i.ID == prog
                                    join p in _db.Parents
                                    on i.ID equals p.dogID
                                    select new SiberianBirthInfoDTO 
                                    {
                                        ID = prog,
                                        PedName = i.pedName,
                                        Titles = i.titles,
                                        DOB = i.DOB,
                                        SireID = p.sireID,
                                        SirePedName = (from s in _db.Siberians
                                                        where s.ID == p.sireID
                                                        select s.pedName).SingleOrDefault(),
                                        DamID = p.damID,
                                        DamPedName = (from s in _db.Siberians
                                                        where s.ID == p.damID
                                                        select s.pedName).SingleOrDefault(),
                                        SireTitles = (from s in _db.Siberians
                                                        where s.ID == p.sireID
                                                        select s.titles).SingleOrDefault(),
                                        DamTitles = (from s in _db.Siberians
                                                        where s.ID == p.damID
                                                        select s.titles).SingleOrDefault()
                                    }).SingleOrDefault();

                    tempLitter.DamID = info.DamID;
                    tempLitter.SireID = info.SireID;
                    tempLitter.DOB = info.DOB;
                    tempLitter.DamPedName = info.DamPedName;
                    tempLitter.SirePedName = info.SirePedName;
                    tempLitter.DamTitles = info.DamTitles;
                    tempLitter.SireTitles = info.SireTitles;
                    tempLitter.Progeny = new List<SiberianSimpleDTO>();

                    bool exists = tempLitters.Any(item => item.DamID == tempLitter.DamID && 
                                                                item.SireID == tempLitter.SireID &&
                                                                item.DOB == tempLitter.DOB);

                    if (!exists)
                    {
                        tempLitters.Add(tempLitter);
                    }

                    foreach (var litter in tempLitters)
                    {
                        if (litter.DamID == info.DamID &&
                            litter.SireID == info.SireID &&
                            litter.DOB == info.DOB)
                        {
                            SiberianSimpleDTO puppy = new SiberianSimpleDTO();
                            puppy.ID = prog;
                            puppy.PedigreeName = info.PedName;
                            puppy.Titles = info.Titles;

                            litter.Progeny.Add(puppy);
                        }
                    }
                }
            }

            return tempLitters;
        }

        public IEnumerable<BreederSimpleDTO> GetBreeders()
        {
            var breeders = (from b in _db.Kennels
                            select new BreederSimpleDTO
                            {
                                ID = b.ID,
                                breederName = (from o in _db.Owners
                                                join bo in _db.BreederName
                                                on o.ID equals bo.ownerID
                                                where b.ID == bo.kennelID
                                                select o.name).ToList(),
                                kennelName = b.kennelName,
                                firstLitter = null,
                                latestLitter = null,
                                active = b.active
                            }).ToList();

            foreach (var breeder in breeders)
            {
                var litters = (from k in _db.Kennels
                                join n in _db.BreederName
                                on k.ID equals n.kennelID
                                join b in _db.DogBreeder
                                on n.ownerID equals b.breederID
                                join s in _db.Siberians
                                on b.dogID equals s.ID
                                where k.ID == breeder.ID
                                orderby s.DOB
                                select s.DOB).ToList();
                
                if (litters.Count() != 0)
                {
                    breeder.firstLitter = litters[0];
                    breeder.latestLitter = litters[litters.Count() - 1];
                }
            }

            return breeders;
        }

        public IEnumerable<CountryDTO> GetCountries()
        {
            var countries = (from c in _db.Countries
                            select new CountryDTO
                            {
                                countryCode = c.id,
                                country = c.value
                            }).ToList();
            
            return countries;
        }

        public IEnumerable<SiberianDTO> GetAllSiberians()
        {
            var siberians = (from s in _db.Siberians
                                join g in _db.Genders
                                on s.gender equals g.shortGender into gen
                                from g in gen
                                orderby s.ID
                                select new SiberianDTO
                                {
                                    ID = s.ID,
                                    Status = s.status,
                                    Titles = s.shortTitles,
                                    PedigreeName = s.pedName,
                                    DOB = s.DOB,
                                    HD = s.HD,
                                    Eye = (from e in _db.EyeExams
                                            where s.ID == e.dogID
                                            orderby e.date descending
                                            select e.date).FirstOrDefault(),
                                    Dead = s.dead,
                                    Year = s.year,
                                    BreedingStatus = s.breedingStatus,
                                    EyeValid = false
                                }).ToList();
            
            return siberians;
        }
    }
}