using System.Collections.Generic;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;
using SHDatabase.Models.ViewModels;

namespace SHDatabase.Repositories
{
    public interface IOwnersRepository
    {
        IEnumerable<OwnerListDTO> GetOwnerList();
        IEnumerable<OwnerAgreementDTO> GetOwnerAgreements();
        bool PostOwnerAgreements(OwnerAgreementViewModel newAgreement);
        void PostOwnerDogConnection(DogOwner newConnection);
        int PostOwner(Owner newOwner);
        void PostBreederDogConnection(DogBreeder newConnection);
    }
}