﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SHDatabase.Models;
using SHDatabase.Models.EntityModels;

namespace SHDatabase.Repositories
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<BraceShowResult>()
                .HasKey(b => new {b.showID, b.M_ID, b.F_ID});

            builder.Entity<BreederShowResult>()
                .HasKey(b => new {b.showID, b.breederID});

            builder.Entity<Country>()
                .HasKey(b => b.id);

            builder.Entity<DogShowResult>()
                .HasKey(b => new {b.showID, b.dogID});

            builder.Entity<EyeDescription>()
                .HasKey(b => b.ID);

            builder.Entity<EyeExam>()
                .HasKey(b => b.ID);

            builder.Entity<EyeResult>()
                .HasKey(b => new {b.examID, b.result});

            builder.Entity<Gender>()
                .HasKey(b => b.shortGender);

            builder.Entity<Image>()
                .HasKey(b => new {b.dogID, b.imageLink});

            builder.Entity<Parents>()
                .HasKey(b => b.dogID);

            builder.Entity<ProgenyShowResult>()
                .HasKey(b => new {b.showID, b.dogID});

            builder.Entity<ShowClass>()
                .HasKey(b => b.showClass);

            builder.Entity<ShowGrade>()
                .HasKey(b => b.grade);

            builder.Entity<ShowType>()
                .HasKey(b => b.type);

            builder.Entity<Status>()
                .HasKey(b => b.status);
                
            builder.Entity<Kennel>()
                .HasKey(b => b.ID);

            builder.Entity<Owner>()
                .HasKey(b => b.ID);

            builder.Entity<DogOwner>()
                .HasKey(b => new {b.dogID, b.ownerID});
            
            builder.Entity<BreederName>()
                .HasKey(b => new {b.kennelID, b.ownerID});

            builder.Entity<DogBreeder>()
                .HasKey(b => new {b.dogID, b.breederID});
        }

        public DbSet<SiberianHusky> Siberians { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<BraceShowResult> BraceShowResults { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<DogShowResult> DogShowResults { get; set; }
        public DbSet<EyeDescription> EyeDescription { get; set; }
        public DbSet<EyeExam> EyeExams { get; set; }
        public DbSet<EyeResult> EyeResults { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Parents> Parents { get; set; }
        public DbSet<ProgenyShowResult> ProgenyShowResults { get; set; }
        public DbSet<ShowClass> ShowClass { get; set; }
        public DbSet<ShowGrade> ShowGrades { get; set; }
        public DbSet<Shows> Shows { get; set; }
        public DbSet<ShowType> ShowTypes { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<Kennel> Kennels { get; set; }
        public DbSet<BreederName> BreederName { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<DogOwner> DogOwner { get; set; }
        public DbSet<BreederName> BreederNames { get; set; }
        public DbSet<DogBreeder> DogBreeder { get; set; }
    }
}
