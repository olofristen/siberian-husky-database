using System.Collections.Generic;
using SHDatabase.Models.DTOModels;
using SHDatabase.Models.EntityModels;

namespace SHDatabase.Repositories
{
    public interface ISiberiansRepository
    {
        IEnumerable<SiberianDTO> GetSiberians();
        IEnumerable<SiberianDTO> GetAllSiberians();
        IEnumerable<SiberianSimpleDTO> GetIcelandicSiberians();
        SiberianDetailsDTO GetSiberianById(int id);
        ParentsIDDTO GetParentsOfSiberianByID(int dogID);
        IEnumerable<LitterSimpleDTO> getProgeny(int dogId);
        IEnumerable<ParentsIDDTO> getProgenyList(int id);
        IEnumerable<SiberianSimpleDTO> GetMales();
        IEnumerable<SiberianSimpleDTO> GetFemales();
        int PostBreeder(Kennel newBreeder);
        BreederDetailDTO GetBreederById(int breederId);
        IEnumerable<BreederSimpleDTO> GetBreeders();
        IEnumerable<CountryDTO> GetCountries();
        int PostSiberian(SiberianHusky newSiberian);
        void PostParents(Parents parents);
        IEnumerable<LitterSimpleDTO> GetBreedersLitters(List<int> puppies);
        int PostEyeExam(EyeExam newExam);
        void PostEyeResult(EyeResult newEyeResult);
    }
}